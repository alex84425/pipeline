#/home/shepherd71c/miniconda3/envs/exome-seq/lib/python3.6/site-packages/neoepiscope/paths.py
ln -s  ../GATK_Mutect2_412_isec/bf_isec/001_PDX.vcf ./001.GATK_S.vcf
ln -s  ../GATK_germline/001N.vcf  ./001.GATK_G.vcf

neoepiscope swap -i 001.GATK_S.vcf -o 001.GATK_S.vcf.sw
neoepiscope prep -v 001.GATK_merge.vcf --o 001.GATK_merge.vcf.adjust


#neoepiscope call -b hg19 -c 001.GATK_merge.vcf.adjust  -a "A*24:02" -p netMHCpan 4 affinity,rank -o 001.GATK_merge.Neo.tsv

#neoepiscope merge -g 001.GATK_G.vcf -s 001.GATK_S.vcf.sw  -o 001.GATK_merge.vcf -t 001T_merge_001

#java -jarGenomeAnalysisTK.jar -T ReadBackedPhasing -R human_g1k_v37.fasta -I A.bam --variant A.vcf -o SNPs_phased.vcf.gz --phaseQualityThresh 20
