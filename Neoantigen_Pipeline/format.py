import pandas as pd
import numpy as np
import sys, os
def rename_affinty( cols ) :
	rename_cols = []  
	for ele in cols:
		if "affinity" in ele:
			rename_cols.append( ele.split("_")[-1] )
		elif "DAI" in ele:
			rename_cols.append( ele.split(" ")[-1] )
		elif "score" in ele:
			rename_cols.append( "_".join(ele.split("_")[-2:])  )
	return rename_cols
def label_AA(x):
	#if len(x["Paired_normal_epitope"] ) !=0:
	if type(x["Paired_normal_epitope"] ) !=type(0.1):
		for i,ele in enumerate(x["Neoepitope"]):
			mut_seq = x["Neoepitope"]
			if x["Neoepitope"][i] != x["Paired_normal_epitope"][i]:
				return mut_seq[:i] + "'"+  mut_seq[i] + "'" + mut_seq[i+1:]
	return ""


#011_GATK/011.NeOout.tsv_output/011.NeOout.tsv.all_info.tsv
#011_varscanHC/011.NeOout.tsv_output/011.NeOout.tsv.all_info.tsv
df = pd.read_csv(sys.argv[1] ,sep="\t")
df.drop_duplicates(subset ="Neoepitope", keep = "first", inplace = True)
#df.drop_duplicates(subset ="Neoepitope", keep = False, inplace = True)
#print( df[df["Neoepitope"] =="SVNNPILRRRK" ] )
#input()

TPM_col  = [ ele for ele  in  list(df.columns) if "CRC" in ele] [0]
df2 = pd.read_csv(sys.argv[1].replace("varscanHC","GATK") ,sep="\t")
df2.drop_duplicates(subset ="Neoepitope", keep = "first", inplace = True)
#df2.drop_duplicates(subset ="Neoepitope", keep = False, inplace = True)

isec = True
#isec = False
Filter = True
#Filter = False
if isec == True:
	df = pd.merge( df, df2, left_on = "Neoepitope", right_on= "Neoepitope", how = "inner")
	df = df[ [ele for ele in list(df.columns) if "_x" in ele or ele =="Neoepitope" ]  ]
	rename_d = dict(zip( list(df.columns) ,  [ ele.replace("_x","") for ele in list(df.columns) ] ))
	df = df.rename( columns = rename_d  )
	df.drop_duplicates(subset ="Neoepitope", keep = "first", inplace = True)
	print( "intersect!!!")
 
df["Neoepitope_label"] = df.apply( lambda x: label_AA(x), axis = 1  )


"Gene	Chr.	Pos.	Ref.	Alt.	Mutation type	TPM	Seq.		Seq."
col = list(df.columns)
""" select HLA to represnet"""
HLA_col = [ele.split(" ")[-1] for ele in col if "Wildtype" in ele]
HLA_col = [ele.split(" ")[-1].split("_")[0] for ele in col if "Wildtype" in ele]
print( HLA_col)

HLA_a_col = [ele for ele in col if HLA_col[0]  in ele]
HLA_all_col = [ [ele for ele in col if HLA  in ele]  for HLA in HLA_col  ]
HLA_all_col  = np.array( HLA_all_col ).reshape([5*len(HLA_all_col)])
HLA_all_col =  list(HLA_all_col )

#print( HLA_all_col)
#print( rename_affinty(HLA_all_col) )
""" filter """
print( "df len:", len(df))
HLA = HLA_col[0]
if isec == True:
	HLA_all_col = [ele for ele in col if HLA  in ele]


affinity_col = "netMHCpan4_Wildtype " + HLA + "_affinity"
DAI_col = "netMHCpan4_" + HLA + " DAI"
bi_col = HLA + "_binding_score"
im_col = HLA + "_immunogenic_score"
if Filter == True:
	df = df[ df[affinity_col] < 1000 ] 
	df = df[ df[ bi_col] > 0.5 ] 
	df = df[ df[ im_col] < 0.5 ] 
	df = df[ df[DAI_col]  > 0 ] 
	df = df[ df[TPM_col]  > 2 ] 
	print( "df len:", len(df))


#input()

ori_col =["gene", "Chromosome", "Pos", "Ref","Alt","Mutation_type", TPM_col,"Neoepitope","Neoepitope_label","Paired_normal_epitope"] + HLA_all_col
rename_col = ["Gene", "Chr.","Pos.","Ref.", "Alt.","Mutation_type", "TPM","Seq_M.","Seq_M_label","Seq_W."] + rename_affinty(HLA_all_col)
rename_d = dict(zip(ori_col, rename_col))
df = df[ori_col]
df = df.rename( columns = rename_d  )

tool = sys.argv[1].split("/")[0].split("_")[-1]
if isec == True:
	df.to_csv( sys.argv[1].split("/")[-1].replace(".all_info.tsv",".all_info_isec_formated.tsv") ,sep = "\t", index = False)
else:
	df.to_csv( sys.argv[1].split("/")[-1].replace(".all_info.tsv",".all_info_"+tool+"_formated.tsv") ,sep = "\t", index = False)

#print( df[ rename_col + HLA_a_col ].iloc[0:10,0:10] )


#print( df )\
"netMHCpan4_Wildtype HLA-A*11:01_affinity"
"netMHCpan4_HLA-A*11:01 DAI"
"HLA-A*11:01_binding_score"
