
ID=$1
#:'
ln -s ../GATK_germline/"$ID"N.vcf ./"$ID".varscanHC_G.vcf

#cat ../varscan_file/"$ID".vcf.*.Somatic.hc
cat ../varscan_file/"$ID".vcf.*.Somatic.hc  | sort -k1,1 -k2,2n | grep -v ^\# > ./"$ID".varscanHC_S.vcf
cat ../varscan_file/"$ID".vcf.*.Somatic.hc | grep  ^\#    | cat - ./"$ID".varscanHC_S.vcf > temp && mv temp ./"$ID".varscanHC_S.vcf

neoepiscope swap -i ./"$ID".varscanHC_S.vcf -o ./"$ID".varscanHC_S.vcf.sw


neoepiscope merge -g "$ID".varscanHC_G.vcf -s "$ID".varscanHC_S.vcf.sw -o "$ID".varscanHC_merge.vcf

extractHAIRS --bam ../GATK_recal/"$ID"T.recal.bam  --VCF "$ID".varscanHC_merge.vcf  --out "$ID".varscanHC_merge.fg --indels 1

HAPCUT2 --fragments     "$ID".varscanHC_merge.fg --VCF "$ID".varscanHC_merge.vcf --output "$ID".varscanHC_merge.hp

neoepiscope prep -v "$ID".varscanHC_merge.vcf  -c "$ID".varscanHC_merge.hp -o "$ID".varscanHC_merge.adjust
#'

#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-A*02:01,HLA-A*11:01,HLA-A*02:07" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo.tsv


#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-A*02:01,HLA-A*11:01,HLA-A*02:07" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo.tsv


#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-A*11:01,HLA-A*02:01,HLA-A*02:07,HLA-B*46:01,HLA-B*51:02,HLA-C*15:02,HLA-C*01:02" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo.tsv
#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-B*46:01,HLA-B*51:02" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo_re.tsv
#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-A*02:07" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo_0207.tsv
#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-A*11:01" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo_1101.tsv

#neoepiscope call -b hg19 -c "$ID".varscanHC_merge.adjust  -a "HLA-B*46:01" -p netMHCpan 4 affinity,rank -o "$ID".varscanHC_merge.Neo_4601.tsv


