from __future__ import print_function
import argparse
from pathlib import Path
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
import pandas as pd 
#from Bio import SeqIO

def md_path(path):
		""" map_docker_path """
		"/home/shepherd71c/workplace/neoantigen/CRC_patient/merge_variant"
		home = str(Path.home())
		p = str( Path(path).resolve() )
		convert_path = p.replace(home, "/opt")
		print( convert_path )
		return convert_path
		pass


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ~/pipeline/Neoantigen_Pipeline/Neo_processing.py  --i 012.varscanHC.NeoOut.tsv  --o_dir 012.varscanHC.NeoOut/ --exp TPM_table.csv  --exp_col CRC3 --r')

	
		parser.add_argument(	'--i' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		parser.add_argument(	'--exp' ,type = str, 
					default = "TPM_table.csv",
					help='input TPM csv file')
		parser.add_argument(	'--exp_col' ,type=str, 
					help='specific columns in TPM csv file ')

		parser.add_argument(	'--f' , action="store_true", default = False,
					help='reuse previous output!')
		parser.add_argument(	'--r' , action="store_true", default = False,
					help='reuse previous output!')

		self.args = parser.parse_args(args)

	def main(self):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
#		file_list = glob.glob("{}/{}".format( self.args.i_dir, self.args.key ))


		

		"Neoepitope  Chromosome  Pos Ref Alt Mutation_type   VAF Neoepitope   Warnings    Transcript_ID   Transcript_type Gene_ID Gene_name   IEDB_ID netMHCpan4_HLA-A*02:01_affinity netMHCpan4_HLA-A*02:01_rank"

		""" step 0 : init """
		df = pd.read_csv( self.args.i, sep="\t", skiprows = 0)
		self.HLA_list = [ ele.split("_")[1] for ele in list(df.columns) if "affinity" in ele ] 
		self.HLA_list = [ 
							"HLA-A*02:01",
							"HLA-A*11:01",
							"HLA-A*24:02"
						]
		

		os.system( "mkdir -p {}".format( self.args.o_dir ) ) 
		print("HLA_list: ", self.HLA_list)
		#HLA
		""" step 1 : filter affinity """
		'''
		def filter_affinity(x):
			pass_count = 0
			for af in self.HLA_af_list:
				pass_count += 1
			return pass_count
			pass
		df["pass"] = df.apply( lambda x : filter_affinity(x) , axis = 1 )
		df = df[ df["pass"] >=1   ]
		print( df["pass"]) 
		'''
		""" step 2 : add wild type """
		df = self.add_wild_type_df( df )
		""" step 3 : add deepHLApan """
		df = self.add_deep_af( df )
		""" step 4 : add TPM info"""
#		df = self.add_exp_info( df )
		""" step 5 : output processing """
		'Neoepitope	Neoepitope	gene	Chromosome	Pos	Ref	Alt	Mutation_type'	
		'netMHCpan4_HLA-A*24:02_affinity	netMHCpan4_HLA-B*40:02_affinity	netMHCpan4_HLA-B*54:01_affinity	netMHCpan4_Wildtype HLA-A*24:02_affinity	netMHCpan4_Wildtype HLA-B*40:02_affinity	netMHCpan4_Wildtype HLA-B*54:01_affinity	netMHCpan4_HLA-A*24:02 DAI	netMHCpan4_HLA-B*40:02 DAI	netMHCpan4_HLA-B*54:01 DAI	HLA-A*24:02_binding_score	HLA-A*24:02_immunogenic_score	HLA-B*40:02_binding_score	HLA-B*40:02_immunogenic_score	HLA-B*54:01_binding_score	HLA-B*54:01_immunogenic_score	CRC1'

		'''
		def label_AA(x):
			if len(x["Neoepitope"] ) !=0:
				for i,ele in enumerate(x["Neoepitope"]):
					mut_seq = x["Neoepitope"]
					if x["Neoepitope"][i] != x["Neoepitope"][i]:
						return mut_seq[:i] + "'"+  mut_seq[i] + "'" + mut_seq[i+1:]
			return ""


		df["Neoepitope_label"] = df.apply( lambda x: label_AA(x), axis = 1  )
		HLA_relate_col = [ ele for ele in list(df.columns) if "HLA" in ele ]
		print( HLA_relate_col )
		ori_col = (["Gene_name","Chromosome","Pos","Ref","Alt","Mutation_type","Neoepitope","Neoepitope_label","Neoepitope"] 
			+ [ self.args.exp_col]
			+ HLA_relate_col)
		concensus_col = ["Gene","Chr.","Pos.","Ref.","Alt.","Mut_type","Mut_Seq.","Mut_Seq_label.","wild_Seq."]
		rename_col = (concensus_col
			+ [  self.args.exp_col + "_TPM"  ]
			+ [ ele.replace("netMHCpan4_", "") for ele in HLA_relate_col  ] )

		rename_d = dict(zip(ori_col, rename_col))
		df = df.rename( columns = rename_d  )
		df = df[ rename_col ]
		""" step 6 : output """
		input()
		f_output = self.args.o_dir + "/" + self.args.i.replace(".tsv","_all.tsv")
		df.to_csv( f_output , sep="\t", index = False)

		print( df.columns)
		for HLA in self.HLA_list:
			single_HLA_col =   [ ele for ele in list(df.columns) if HLA in ele ]
			output_col = (concensus_col
				+ [  self.args.exp_col + "_TPM"  ]
				+ single_HLA_col )
			
			HLA_output = self.args.o_dir + "/" + HLA.replace("*","").replace(":","") + "_Neo_output.tsv"

			df[ output_col ].to_csv( HLA_output , sep="\t", index = False)

			
			print(HLA)
		'''

	def add_exp_info(self, df):
		exp_col = self.args.exp_col
		exp_df = pd.read_csv(self.args.exp, sep=",")
		#exp_df = exp_df[["gene_synmbol", exp_col,"gene_id"] ]
		exp_df = exp_df[["gene_synmbol", exp_col] ]

		exp_df.drop_duplicates(subset ="gene_synmbol", keep = False, inplace = True) 

		df["Gene_name"] = df["Gene_name"].apply( lambda x : x.split(";")[0] )
		df = pd.merge(df, exp_df, left_on = "Gene_name", right_on = "gene_synmbol", how = "left")
		#df = pd.merge(df, exp_df, left_on = "Gene_ID", right_on = "gene_id", how = "left")
		print("df len :",len(df))
		#print(df[exp_col])
		match = len(df.dropna( subset=[exp_col] ) )
		print( "with TPM: ", round(match/len(df),2 ) )
		df = df.drop(columns=['gene_synmbol'])
		return df 

		
		pass

	def add_deep_af(self, df):
		for HLA in self.HLA_list:
			HLA_ori = HLA
			HLA = HLA.replace("*","")

			""" prepare input """
			tmp_wild_pep = self.args.o_dir + "/" + HLA + "_mut_pep.csv"
			#tmp_wild_pep = self.args.i
			# Annotation,HLA,peptide
			tmp_input_df = df[ ["Neoepitope"] ].rename( columns={ "Neoepitope":"peptide" } )
			tmp_input_df["HLA"] = HLA
			tmp_input_df["Annotation"] = tmp_input_df.index
			tmp_input_df[ ["Annotation","HLA","peptide"] ].to_csv(tmp_wild_pep, sep = ",", index = False, header=True)
			print("tmp_wild_pep: ",tmp_wild_pep)
			# 012.varscanHC.NeoOut//HLA-A02:01_mut_pep.tsv
			container_ID = "07e59a7bb463"
			cmd = "docker exec {} deephlapan -F {} -O {} ".format( container_ID, md_path(tmp_wild_pep), md_path(self.args.o_dir))
			#if self.args.r ==False:
			if True:
				print( cmd )
#				os.system( cmd )
#				input()

			tmp_output = tmp_wild_pep.replace(".csv","_predicted_result.csv")

			""" read output"""
			# Annotation,HLA,Peptide,binding score,immunogenic score
			tmp_df = pd.read_csv( tmp_output, sep=",", skiprows=0)
			tmp_df.drop_duplicates(subset ="Peptide", keep = False, inplace = True) 

			tmp_df = tmp_df[ [ "Peptide","binding score","immunogenic score"] ] 
			tmp_df = tmp_df.rename(columns={
				"binding score": "netMHCpan4_"+HLA_ori+"_binding score", 
				"immunogenic score": "netMHCpan4_"+HLA_ori+"_immunogenic score" })
#			print("tmp_df len", len( tmp_df))
#			print("df len", len(df))

			df = pd.merge(df, tmp_df, left_on = "Neoepitope", right_on = "Peptide", how = "left")
			df = df.drop(columns=['Peptide'])
#			print("df len", len(df))
#			input()
			print(df.columns)
			df.to_csv("full_output.tsv", sep="\t", index = False)
		return df


		pass

	def add_wild_type_df(self, df):
		""" create input"""
#		print(df["Neoepitope"])
		#df = df.dropna( subset=['Neoepitope'] )
#		df = df.dropna( subset=['Neoepitope'] )
		#tmp_wild_pep = self.args.o_dir + "/" + "wild_pep.tsv"
#		tmp_wild_pep = self.args.o_dir + "/" + "wild_pep.tsv"
#		df["Neoepitope"].to_frame().to_csv(tmp_wild_pep, sep = "\t", index = False, header=False)
		tmp_wild_pep = self.args.i
	

		for HLA in self.HLA_list:
			HLA_ori = HLA
			HLA = HLA.replace("*","")
			tmp_wild_pep_output = self.args.i.replace(".tsv","_"+HLA+".tsv" )
			cmd = "/home/shepherd71c/git_file/netMHCpan-4.0/netMHCpan -BA -a {} -inptype 1 -p -xls -xlsfile {} {}  ".format(HLA, tmp_wild_pep_output, tmp_wild_pep)

			print( cmd ) 
#			input()
			if self.args.r == False:
				print( cmd )
				os.system( cmd )
				
			tmp_df = pd.read_csv( tmp_wild_pep_output, sep="\t", skiprows=1)
			tmp_df = tmp_df[["Peptide","nM","Rank"] ]
			tmp_df.drop_duplicates(subset ="Peptide", keep = False, inplace = True) 
			"netMHCpan4_HLA-A*02:01_affinity"
			tmp_df = tmp_df.rename(columns={
				"nM": ""+"netMHCpan4_"+HLA_ori+"_affinity", 
				"Rank": ""+"netMHCpan4_"+HLA_ori+"_Rank" })
#			print("unique len:",  len(list(set(list(tmp_df["Peptide"]))))  )
#			print("tmp_df len", len( tmp_df))
#			print("df len", len(df))

			df = pd.merge(df, tmp_df, left_on = "Neoepitope", right_on = "Peptide", how = "left")
			df = df.drop(columns=['Peptide'])
#			print("df len", len(df))
#			print(df.columns)

#		netmhcpan,"-BA","-a",allele,"-inptype","1","-p","-xls","-xlsfile",mhc_out,peptide_file,
		df.to_csv("output_three_HLA.tsv", sep = "\t", index = False)
		return df
		pass


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


