from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
from os.path import expanduser
home = expanduser("~")
sys.path.append(home + "/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )
import mygene

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ~/pipeline/Neoantigen_Pipeline/rsem_with_genesyb.py --i_dir ./ --key "008.Quant.genes.results" --o_dir TPM_with_syb/')
	
		parser.add_argument(	'--i1' ,type=str, 
					help='input directory')
		parser.add_argument(	'--i2' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		""" specific optopm"""
		parser.add_argument(	'--f' ,type=str,
					default="",
					help='input directory')


		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		self.args = parser.parse_args(args)

	def main(self):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		"""
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		
		os.system("mkdir -p "+self.args.o_dir)
		"""
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2

		df1 = pd.read_csv( self.args.i1 ,sep = ",")
		df2 = pd.read_csv( self.args.i2 ,sep = ",")

		print(df1.columns)
		print(df2.columns)
		input()

		print(len(df1))
		df1 = pd.merge( df1, df2, left_on = "gene_id", right_on = "gene_id" ,how = "outer" )
		print(len(df1))

		"""
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			df = pd.read_csv( file ,sep = "\t")
#			df = df.iloc[1:10,:]
			df["merge_col"] = df['gene_id'].apply( lambda x :x.split(".")[0] ) 
#			print( df.columns)
			gene_id_list = [ ele.split(".")[0] for ele in list(df['merge_col']) ] 
			mg = mygene.MyGeneInfo()
			g_df = mg.getgenes( gene_id_list, fields=['symbol'], as_dataframe=True  )
			df = pd.merge( df, g_df, left_on = "merge_col", right_on = "query" ,how = "left" )
			df = df.drop(['_id', '_score'], axis=1)
			df = df.rename( columns = {"symbol":"gene_symbol", "TPM":"CRC"+(f_n[0:3].lstrip("0")) }  )
			output = "{}/{}.csv".format( self.args.o_dir, f_n)

			print("output:",output)
			df.to_csv( output, sep = ",", index = False )
#			print( df )
						
#			t_list.add_thread( cmd )
#		t_list.start_thread()
		"""

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


