from __future__ import print_function
import argparse
from pathlib import Path
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
import pandas as pd 
import numpy as np
#from Bio import SeqIO

def md_path(path):
		""" map_docker_path """
		"/home/shepherd71c/workplace/neoantigen/CRC_patient/merge_variant"
		home = str(Path.home())
		p = str( Path(path).resolve() )
		convert_path = p.replace(home, "/opt")
		print( convert_path )
		return convert_path
		pass


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ~/pipeline/Neoantigen_Pipeline/Neo_processing.py  --i 012.varscanHC.NeoOut.tsv  --o_dir 012.varscanHC.NeoOut/ --exp TPM_table.csv  --exp_col CRC3 --r')

	
		parser.add_argument(	'--i' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		parser.add_argument(	'--exp' ,type = str, 
					default = "TPM_table.csv",
					help='input TPM csv file')
		parser.add_argument(	'--exp_col' ,type=str, 
					help='specific columns in TPM csv file ')

		parser.add_argument(	'--f' , action="store_true", default = False,
					help='reuse previous output!')
		parser.add_argument(	'--r' , action="store_true", default = False,
					help='reuse previous output!')

		self.args = parser.parse_args(args)

	def main(self):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
#		file_list = glob.glob("{}/{}".format( self.args.i_dir, self.args.key ))
		"Neoepitope  Chromosome  Pos Ref Alt Mutation_type   VAF Paired_normal_epitope   Warnings    Transcript_ID   Transcript_type Gene_ID Gene_name   IEDB_ID netMHCpan4_HLA-A*02:01_affinity netMHCpan4_HLA-A*02:01_rank"

		""" step 0 : init 
			(1) remove dupplicate HLA column
		"""
		df = pd.read_csv( self.args.i, sep="\t", skiprows = 1)

#		print(df.columns)
		col = [ ele for ele in list(df.columns) if ".1" not in ele]
		col =  list( dict.fromkeys(col) )
		df = df[col]
#		print(df.columns)

		self.HLA_list = [ ele.split("_")[1] for ele in list(df.columns) if "affinity" in ele ] 
		self.HLA_af_list = [ ele for ele in list(df.columns) if "affinity" in ele ] 

		os.system( "mkdir -p {}".format( self.args.o_dir ) ) 
		print("HLA_list: ", self.HLA_list)
		#HLA
		""" step 1 : filter affinity """
		def filter_affinity(x):
			pass_count = 0
			for af in self.HLA_af_list:
				pass_count += 1
			return pass_count
			pass
		df["pass"] = df.apply( lambda x : filter_affinity(x) , axis = 1 )
		df = df[ df["pass"] >=1   ]
		print( df["pass"]) 
		""" step 2 : add wild affinity """
		df = self.add_NetMHCpanV4_affinity( df, "Paired_normal_epitope", "Wild " )
		""" step 2.5 : add mut affinty (rerun due to neoepiscope bug ) """
		df = self.add_NetMHCpanV4_affinity( df, "Neoepitope", "Mut " )

		# replace value
		Mut_col = [ ele for ele in list(df.columns) if "Mut " in ele] 
		ori_Mut_col = [ ele[4:] for ele in list(df.columns) if "Mut " in ele] 
		for i,ele in enumerate(Mut_col):
			if  ori_Mut_col[i]  == Mut_col[i][4:]:
				df[ ori_Mut_col[i] ] = df[ele]

		df = df.drop(columns= Mut_col )
		""" step 3 : add deepHLApan """
		df = self.add_deep_af( df )
		""" step 3.5 : add DAI """

		self.HLA_af_and_wild_list = [ ele for ele in list(df.columns) if "affinity" in ele ]
		for HLA_g in [ [ col for col in self.HLA_af_and_wild_list if HLA in col] for HLA in self.HLA_list] :
#			print(HLA_g)
			DAI_col = HLA_g[0].replace("_affinity", "_DAI")
			df[DAI_col] = df[ HLA_g[1]]  -  df[ HLA_g[0]] 
			

		""" step 4 : add TPM info"""
		df = self.add_exp_info( df )
		""" step 5 : output processing """
		'Neoepitope	Paired_normal_epitope	gene	Chromosome	Pos	Ref	Alt	Mutation_type'	
		'netMHCpan4_HLA-A*24:02_affinity	netMHCpan4_HLA-B*40:02_affinity	netMHCpan4_HLA-B*54:01_affinity	netMHCpan4_Wildtype HLA-A*24:02_affinity	netMHCpan4_Wildtype HLA-B*40:02_affinity	netMHCpan4_Wildtype HLA-B*54:01_affinity	netMHCpan4_HLA-A*24:02 DAI	netMHCpan4_HLA-B*40:02 DAI	netMHCpan4_HLA-B*54:01 DAI	HLA-A*24:02_binding_score	HLA-A*24:02_immunogenic_score	HLA-B*40:02_binding_score	HLA-B*40:02_immunogenic_score	HLA-B*54:01_binding_score	HLA-B*54:01_immunogenic_score	CRC1'

		def label_AA(x):
			#type not equal flaot
			if type(x["Paired_normal_epitope"] ) != type(0.01):
				for i,ele in enumerate(x["Neoepitope"]):
					mut_seq = x["Neoepitope"]
					if x["Neoepitope"][i] != x["Paired_normal_epitope"][i]:
						return mut_seq[:i] + "'"+  mut_seq[i] + "'" + mut_seq[i+1:]
			return ""


		df["Neoepitope_label"] = df.apply( lambda x: label_AA(x), axis = 1  )
		HLA_relate_col = [ ele for ele in list(df.columns) if "HLA" in ele ]

		""" rearrange columns by HLA and HLA_group"""
		HLA_all_col = [ [ele for ele in HLA_relate_col if HLA  in ele]  for HLA in self.HLA_list  ]
#		print( HLA_all_col[0] )
#		print( len(HLA_all_col[0] ))
		HLA_all_col = [ HLA_g[0:4]+[HLA_g[-1]]+HLA_g[4:6]  for HLA_g in HLA_all_col  ]
#		print( HLA_all_col[0] )
#		print( len(HLA_all_col[0] ))
#		input()
		


		HLA_all_col  = np.array( HLA_all_col ).reshape([len(HLA_relate_col) ])
		HLA_all_col =  list(HLA_all_col )
		HLA_relate_col = HLA_all_col
		TPM_col = self.args.exp_col
		ori_col = (["Gene_name","Chromosome","Pos","Ref","Alt","Mutation_type",TPM_col,"Neoepitope","Neoepitope_label","Paired_normal_epitope"] 
			+ HLA_relate_col)
		concensus_col = ["Gene","Chr.","Pos.","Ref.","Alt.","Mut_type",TPM_col+"_TPM","Mut_Seq.","Mut_Seq_label.","wild_Seq."]
		rename_col = (concensus_col
			+ [ ele.replace("netMHCpan4_", "") for ele in HLA_relate_col  ] )



		rename_d = dict(zip(ori_col, rename_col))
		df = df.rename( columns = rename_d  )
		""" skip columns """
		rename_col = [ ele for ele in  rename_col if "rank" not in ele ] 
		df = df[ rename_col ]
		""" step 6 : output """
		print( "\t".join(self.HLA_list) )
		print( df.columns )
#		input()
		f_output = self.args.o_dir + "/" + self.args.i.replace(".tsv","_all.tsv")
		
		self.rename_column_name( df ).to_csv( f_output , sep="\t", index = False)
		""" step 7: add header to short shorten columns display size """
		self.additional_header( self.rename_column_name( df ), f_output )
		self.output_file_list = []
		self.output_file_list.append( f_output )


		print("HLA output:", f_output)

		for HLA in self.HLA_list:
			single_HLA_col =   [ ele for ele in list(df.columns) if HLA in ele ]
			output_col = (concensus_col
#				+ [  self.args.exp_col + "_TPM"  ]
				+ single_HLA_col )
			HLA_output = self.args.o_dir + "/" + HLA.replace("*","").replace(":","") + "_Neo_output.tsv"

			tmp_df = df[output_col]

			""" TPM >2 , affinity < 1000  """
			tmp_df = tmp_df[ tmp_df[HLA + "_affinity"  ] < 1000  ] 
#			tmp_df = tmp_df[ tmp_df[HLA + "_immunogenic score"  ] >0.5  ] 
			tmp_df = tmp_df[ tmp_df[ TPM_col + "_TPM"] > 2 ] 
			""" add iNeo score """
			tmp_df[  HLA + "_iNeo score"] = np.log10(tmp_df[TPM_col+"_TPM"] )* tmp_df[HLA + "_binding score"] * np.sqrt(tmp_df[HLA + "_immunogenic score"]) / tmp_df[HLA + "_affinity"]

			tmp_df = self.groupby_col_and_pick(tmp_df, HLA + "_affinity", way = "min")
			tmp_df = self.groupby_col_and_pick(tmp_df, HLA + "_binding score", way = "max")
			tmp_df = self.groupby_col_and_pick(tmp_df, HLA + "_iNeo score", way = "max")
#			tmp_df = df[ output_col+[HLA + "_affinity",HLA + "_binding score"] ]
#			print( tmp_df.columns)
			tmp_df = self.rename_column_name( tmp_df )
			tmp_df.to_csv( HLA_output , sep="\t", index = False)
			print("single HLA output:", HLA_output)
#			TPM * HLA + "_binding score" * HLA + "_immunogenic score" / HLA + "_affinity"
			self.additional_header( tmp_df , HLA_output )
			self.output_file_list.append( HLA_output )

		""" step 08: to excel """
#		for output_file in self.output_file_list:
		self.to_excel( self.output_file_list)


	def rename_column_name(self, df):
		""" reomove HLA keyword in col"""
#		print( df.columns  )
		for HLA in self.HLA_list:

			HLA_col_ori = [ele for ele in list(df.columns) if HLA in ele]
			HLA_col_rename = [ele.replace(HLA+"_", "") for ele in list(df.columns) if HLA in ele]

			rename_d = dict(zip(HLA_col_ori, HLA_col_rename ))
			df = df.rename( columns = rename_d  )

#		print( df.columns  )
#		input()
		return df

	def additional_header(self,df, output ):

		col = list(df.columns)
		tmp_HLA_list = self.HLA_list.copy()
		""" first line """
		f_line = "\t".join( [ "" if "affinity" != ele else  tmp_HLA_list.pop(0) for i,ele in enumerate(col)] )

#		[x+1 for x in l if x >= 45 else x+5]
		""" second line """
		rename_d = dict(zip(
			["Mut_Seq.","wild_Seq.","affinity","Wild affinity","binding score"],
			["Mutated peptide","Wild-type peptide","netMHCpan4","Wildtype","DeepHLApan"]
		))

		s_line = "\t".join( [ rename_d.get(ele,"")  for i,ele in enumerate(col)] )
#		print(s_line)
		ad_output = self.args.o_dir + "/" +"additional_header.tsv"
		with open(ad_output, "w") as f:
			if "all" in output.split("/")[-1]:
				f.write("#{}\n#{}\n".format(f_line,s_line))
			else:
				f.write("#{}\n#{}\n".format( "\t".join( [ ""  for i,ele in enumerate(col)] ) ,s_line))
		
		add_cmd = "cat {0} | cat - {1} > temp && mv temp {1}".format(ad_output, output)
#		print( add_cmd )
		os.system( add_cmd )
		print("done!!")


	def to_excel(self, output_file_list):
		""" to excel"""
		df_list = []
		for output in output_file_list:
			tmp_df = pd.read_csv(output, sep = "\t" )
			col = list(tmp_df.columns)
			new_col =  [  ele if "Unnamed" not in ele else  "" for i,ele in enumerate(col)] 
			rename_d2 = dict(zip(col, new_col  ))
			tmp_df = tmp_df.rename( columns= rename_d2 )
			df_list.append( tmp_df )

		"""
		mode = 'a'
		if "all" in output.split("/")[-1]:
			self.excel_output = output.replace(".tsv",".xlsx")
			os.system( "rm {}".format(self.excel_output) )
			mode = 'w'
		"""			
		self.excel_output = output_file_list[0].replace(".tsv",".xlsx")
		os.system( "rm {}".format(self.excel_output) )
		mode = 'w'
		print("df_list len:",len(df_list))

		
		with pd.ExcelWriter( self.excel_output, mode= mode
#			,  engine="openpyxl"
			,  engine='xlsxwriter'
			, options={'strings_to_numbers': True}
			) as writer:  
			#HLA-B5401_Neo_output.tsv
			for i,tmp_df in enumerate(df_list):
				if i ==0:
					sheet_name = "{}".format( output_file_list[i].split("/")[-1].replace("_merge.Neo_all.tsv","_Neo_all") )
					tmp_df.to_excel(writer, sheet_name= sheet_name, index = False)
				else:
					sheet_name = "{}".format( output_file_list[i].split("/")[-1].replace("_Neo_output.tsv","") )
					
					tmp_df.to_excel(writer, sheet_name= sheet_name, index = False)
				print( sheet_name)
			print("excel output:{}".format(self.excel_output) )
		pass

	def groupby_col_and_pick(self,df,col, way="min"):
		""" inplace the by column
				step 01: groupby columns and pick one
				step 02: inplace
				step 03: filter by value
		
		"""
		if way == "min":
			g_df =  df.loc[df.groupby(["Chr.","Pos."])[col].idxmin()]
			g_df.sort_values(by=[col], inplace = True, ascending = True)
		else:
			g_df =  df.loc[df.groupby(["Chr.","Pos."])[col].idxmax()]
			g_df.sort_values(by=[col], inplace = True, ascending = False)
		i_list = list(g_df.index)

#		print(i_list)
		
		rank_col = col + "_ranking"
		df[rank_col] = np.nan
		
		for i,index in enumerate(i_list):
			#ele[rank_col] = i+1
			df.loc[index,rank_col] = i+1
#			print(df.loc[index,rank_col] )

#		print( df.loc[i_list,["Mut_Seq.",col, rank_col]] )
		return df
		pass

	def add_exp_info(self, df):
		exp_col = self.args.exp_col
		exp_df = pd.read_csv(self.args.exp, sep=",")
		exp_df = exp_df.dropna( subset=[exp_col] )
		#exp_df = exp_df[["gene_symbol", exp_col,"gene_id"] ]
		exp_df = exp_df[["gene_symbol", exp_col] ]

		exp_df.drop_duplicates(subset ="gene_symbol", keep = "first", inplace = True) 

		df["Gene_name"] = df["Gene_name"].apply( lambda x : x.split(";")[0] )
		df = pd.merge(df, exp_df, left_on = "Gene_name", right_on = "gene_symbol", how = "left")
		#df = pd.merge(df, exp_df, left_on = "Gene_ID", right_on = "gene_id", how = "left")
		print("df len :",len(df))
		#print(df[exp_col])
		match = len(df.dropna( subset=[exp_col] ) )
		print( "with TPM: ", round(match/len(df),2 ) )
		df = df.drop(columns=['gene_symbol'])
		return df 

		
		pass

	def add_deep_af(self, df):
		for HLA in self.HLA_list:
			HLA_ori = HLA
			HLA = HLA.replace("*","")

			""" prepare input """
			tmp_wild_pep = self.args.o_dir + "/" + HLA + "_mut_pep.csv"
			# Annotation,HLA,peptide
			tmp_input_df = df[ ["Neoepitope"] ].rename( columns={ "Neoepitope":"peptide" } )
			tmp_input_df["HLA"] = HLA
			tmp_input_df["Annotation"] = tmp_input_df.index
			tmp_input_df[ ["Annotation","HLA","peptide"] ].to_csv(tmp_wild_pep, sep = ",", index = False, header=True)
			print("tmp_wild_pep: ",tmp_wild_pep)
			# 012.varscanHC.NeoOut//HLA-A02:01_mut_pep.tsv
			cmd = "docker exec 07e59a7bb463  deephlapan -F {} -O {} ".format( md_path(tmp_wild_pep), md_path(self.args.o_dir))
			if self.args.r ==False:
				print( cmd )
				os.system( cmd )
			tmp_output = tmp_wild_pep.replace(".csv","_predicted_result.csv")

			""" read output"""
			# Annotation,HLA,Peptide,binding score,immunogenic score
			tmp_df = pd.read_csv( tmp_output, sep=",", skiprows=0)
			tmp_df.drop_duplicates(subset ="Peptide", keep = "first", inplace = True) 

			tmp_df = tmp_df[ [ "Peptide","binding score","immunogenic score"] ] 
			tmp_df = tmp_df.rename(columns={
				"binding score": "netMHCpan4_"+HLA_ori+"_binding score", 
				"immunogenic score": "netMHCpan4_"+HLA_ori+"_immunogenic score" })
#			print("tmp_df len", len( tmp_df))
#			print("df len", len(df))
			df = pd.merge(df, tmp_df, left_on = "Neoepitope", right_on = "Peptide", how = "left")
			df = df.drop(columns=['Peptide'])
#			print("df len", len(df))
#			input()
			print(df.columns)
			df.to_csv("full_output.tsv", sep="\t", index = False)
		return df


		pass

	def add_NetMHCpanV4_affinity(self, df, input_col, output_col_prefix):
		""" 
			init: select input column, and output col
			(1) create input
			(2) run
			(3) merge to df
		"""
#		input_col = "Paired_normal_epitope"
#		output_col_prefix =  "Wild "

		print(df["Mutation_type"].unique() )
		pep_df = df.dropna( subset=[input_col] )
		#tmp_wild_pep = self.args.o_dir + "/" + "wild_pep.tsv"
		tmp_wild_pep = self.args.o_dir + "/" + output_col_prefix.replace(" ","") + "_pep.tsv"
		pep_df[input_col].to_frame().to_csv(tmp_wild_pep, sep = "\t", index = False, header=False)


		for HLA in self.HLA_list:
			HLA_ori = HLA
			HLA = HLA.replace("*","")
			tmp_wild_pep_output = self.args.o_dir + "/" + HLA+"_" + output_col_prefix.replace(" ","")  + "_pep.tsv"
			#cmd = "/home/shepherd71c/git_file/netMHCpan_4.0/netMHCpan-4.0/netMHCpan -BA -a {} -inptype 1 -p -xls -xlsfile {} {}  ".format(HLA, tmp_wild_pep_output, tmp_wild_pep)
			cmd = "/home/shepherd71c/git_file/netMHCpan-4.0/netMHCpan -BA -a {} -inptype 1 -p -xls -xlsfile {} {}  ".format(HLA, tmp_wild_pep_output, tmp_wild_pep)
			if self.args.r == False:
				print( cmd )
				os.system( cmd )
				
			tmp_df = pd.read_csv( tmp_wild_pep_output, sep="\t", skiprows=1)
			tmp_df = tmp_df[["Peptide","nM","Rank"] ]
			tmp_df.drop_duplicates(subset ="Peptide", keep = "first", inplace = True) 
			"netMHCpan4_HLA-A*02:01_affinity"
			tmp_df = tmp_df.rename(columns={
				"nM": output_col_prefix+"netMHCpan4_"+HLA_ori+"_affinity", 
				"Rank": output_col_prefix+"netMHCpan4_"+HLA_ori+"_rank" })
#			print("unique len:",  len(list(set(list(tmp_df["Peptide"]))))  )
#			print("tmp_df len", len( tmp_df))
#			print("df len", len(df))
			df = pd.merge(df, tmp_df, left_on = input_col, right_on = "Peptide", how = "left")
			df = df.drop(columns=['Peptide'])
#			print("df len", len(df))
#			print(df.columns)
#		netmhcpan,"-BA","-a",allele,"-inptype","1","-p","-xls","-xlsfile",mhc_out,peptide_file,
		return df
		pass


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


