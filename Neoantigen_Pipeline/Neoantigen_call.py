from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
from os.path import expanduser
home = expanduser("~")
sys.path.append(home+"/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		""" specific optopm"""
		parser.add_argument(	'--f' ,type=str,
					default="",
					help='input directory')

		parser.add_argument(	'--HLA_table' ,type=str,
					default="",
					help='input directory')

		parser.add_argument(	'--ID' ,type=str,
					default="",
					help='input directory')


		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')
		parser.add_argument(	'--f_f' ,type=int,
					default=0,
					help="ex: first 30 file")





		self.args = parser.parse_args(args)

	def main(self):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )

		if self.args.f_f!=0:
			file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )[:self.args.f_f]



		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		
#		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
#		for i,file in enumerate(file_list):
#			f_n = file.split("/")[-1]
#			output = self.args.o_dir + "/" + f_n
#			cmd = "fastq_quality_filter -q 30 -i {} -o {}  -p 80 -Q33".format( file, output  )
#			print( cmd)
#			t_list.add_thread( cmd )


			

			
#			ID = f_n[0:3]
		if True:
			ID = self.args.ID
			""" ln -s the file """
#			cmd1  = "ln -s ../GATK_germline/{}N.vcf ./{}.varscanHC_G.vcf".format( ID )
#			cat ../varscan_file/"$ID".vcf.*.Somatic.hc  | sort -k1,1 -k2,2n | grep -v ^\# > ./"$ID".varscanHC_S.vcf
#			cat ../varscan_file/"$ID".vcf.*.Somatic.hc | grep  ^\#    | cat - ./"$ID".varscanHC_S.vcf > temp && mv temp ./"$ID".varscanHC_S.vcf



			""" read HLA table """
			HLA_df = pd.read_csv( self.args.HLA_table, sep = "\t"  )
			HLA_df["ID"] =  HLA_df["Unnamed: 0"] 
			HLA_str = HLA_df [ HLA_df["ID"] == "CRC"+self.args.ID.lstrip('0') ].iloc[:,1:7].to_string(header=False,
                  index=False,
                  index_names=False)
			HLA_str =  [ "HLA-"+ele.replace(" ","") for ele in HLA_str.split("  ") ]
			HLA_str = ",".join(HLA_str)
			print( HLA_str  )

			""" neo call """
			cmd_call = "neoepiscope call -b hg19 -c {0}.varscanHC_merge.adjust  -a \"{1}\" -p netMHCpan 4 affinity,rank -o {0}.varscanHC_merge.Neo.tsv".format( ID, HLA_str )
			print( cmd_call )
#HLA-A02:01' is not a valid allele for netMHCpan
			os.system( cmd_call )

"""
ID=$1
ln -s ../GATK_germline/"$ID"N.vcf ./"$ID".varscanHC_G.vcf

#cat ../varscan_file/"$ID".vcf.*.Somatic.hc
cat ../varscan_file/"$ID".vcf.*.Somatic.hc  | sort -k1,1 -k2,2n | grep -v ^\# > ./"$ID".varscanHC_S.vcf
cat ../varscan_file/"$ID".vcf.*.Somatic.hc | grep  ^\#    | cat - ./"$ID".varscanHC_S.vcf > temp && mv temp ./"$ID".varscanHC_S.vcf

neoepiscope swap -i ./"$ID".varscanHC_S.vcf -o ./"$ID".varscanHC_S.vcf.sw


neoepiscope merge -g "$ID".varscanHC_G.vcf -s "$ID".varscanHC_S.vcf.sw -o "$ID".varscanHC_merge.vcf

extractHAIRS --bam ../GATK_recal/"$ID"T.recal.bam  --VCF "$ID".varscanHC_merge.vcf  --out "$ID".varscanHC_merge.fg --indels 1

HAPCUT2 --fragments     "$ID".varscanHC_merge.fg --VCF "$ID".varscanHC_merge.vcf --output "$ID".varscanHC_merge.hp

neoepiscope prep -v "$ID".varscanHC_merge.vcf  -c "$ID".varscanHC_merge.hp -o "$ID".varscanHC_merge.adjust

"""
			
		
#		t_list.start_thread()

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


