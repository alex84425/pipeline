#https://zhuanlan.zhihu.com/p/47024487
#https://www.jianshu.com/p/9915fce02b10
#https://www.twblogs.net/a/5ccc2b77bd9eee7b833102d0
# iptable: https://stackoverflow.max-everyday.com/2017/09/linux-firewall-iptables/

wget -qO- https://download.asperasoft.com/download/sw/connect/3.8.1/ibm-aspera-connect-3.8.1.161274-linux-g2.12-64.tar.gz | tar xvz

## run it
chmod +x ibm-aspera-connect-3.8.1.161274-linux-g2.12-64.sh
./ibm-aspera-connect-3.8.1.161274-linux-g2.12-64.sh

## add it to the path now and in the future
export PATH=$PATH:~/.aspera/connect/bin/
echo 'export PATH=$PATH:~/.aspera/connect/bin/' >> ~/.bashrc

#example
ascp  -P 33001 -i ~/.aspera/connect/etc/asperaweb_id_dsa.openssh era-fasp@fasp.sra.ebi.ac.uk:/vol1/fastq/SRR346/SRR346368/SRR346368.fastq.gz .
