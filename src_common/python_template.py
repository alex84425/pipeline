from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		""" specific optopm"""
		parser.add_argument(	'--f' ,type=str,
					default="",
					help='input directory')


		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		self.args = parser.parse_args(args)

	def main(self):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			output = self.args.o_dir + "/" + f_n
			cmd = "fastq_quality_filter -q 30 -i {} -o {}  -p 80 -Q33".format( file, output  )
			print( cmd)
			t_list.add_thread( cmd )
			
		
		t_list.start_thread()

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


