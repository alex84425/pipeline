def list_iexclude( l, key,  option = "ex" ):
	""" 
	ex: file_list   list_iexclude( file_list, "fastq", "in" )
	"""
	key_list = []
	if type(key) == type(""):
		key_list = key.split(",")


	if option == "ex" :
		for key in key_list:
			l = [ele for ele in l if key not in ele ]
		return l
	else:
		tmp_l = []
		for ele in l:
			b = 0
			for key in key_list:
				if key in ele:
					b = 1 
					break
			if b == 1 :
				tmp_l.append(ele)
	
	
		return tmp_l
		
