from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		""" specific optopm"""
		parser.add_argument(	'--f' ,type=str,
					default="",
					help='input directory')
		parser.add_argument(	'--exon_bed' ,type=str,
				   default="region_bed/S07604514_AllTracks.bed",
					help='input bed file')



		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		self.args = parser.parse_args(args)

	def main(self,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 0
		total_count = 0
		total_pass_count = 0

		""" run !!"""
#
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			print(f_n)

			input1 = file
			output1 = self.args.o_dir+ "/" + f_n
			cmd1 = "bedtools intersect -a {} -b {} > {}".format(file, self.args.exon_bed ,output1)
#			print( cmd1)
#			os.system( cmd1 )
			t_list.add_thread( cmd1 )
		t_list.start_thread()

		""" print and output !!"""
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
#			print(f_n)
			input1 = file
			output1 = self.args.o_dir+ "/" + f_n
			cmd1 = "bedtools intersect -a {} -b {} > {}".format(file, self.args.exon_bed ,output1)
#			print( cmd1)
#			os.system( cmd1 )

			if ".vcf" in  file:

				self.vcf_add_header( file, output1)
			count =  self.vcf_record_count( file )	
			pass_count =  self.vcf_record_count( output1 )	
			total_count +=  count
			total_pass_count +=  pass_count
			print( "{}: {} -> {}, ({}%)".format(f_n, count, pass_count, round(100*pass_count/count,2 ) ) )
		print( "avg: {} -> {}, ({}%)".format(total_count, total_pass_count, round(100*total_pass_count/total_count,2 ) ) )
#			eprint("processing file: ",file)

	def read_vcf_header(self,vcf):
		""" https://stackoverflow.com/questions/32224363/python-convert-comma-separated-list-to-pandas-dataframe i """
		""" CHROM  POS ID  REF ALT QUAL FILTER  INFO	FORMAT  NORMAL  TUMOR """
		vcf_list = []
		with open(vcf, "r") as f:
			lines = f.readlines()
			for i,line in enumerate(lines):
				if line[0]!="#":
					header_loc = i-1
					break
#			vcf_content = lines[header_loc+1:]
#		   vcf_content = vcf_content[:10]
			vcf_header = "".join(  lines[ :header_loc+1] )
			return vcf_header	
	def vcf_record_count(self,vcf):
		""" https://stackoverflow.com/questions/32224363/python-convert-comma-separated-list-to-pandas-dataframe i """
		""" CHROM  POS ID  REF ALT QUAL FILTER  INFO	FORMAT  NORMAL  TUMOR """
		vcf_list = []
		with open(vcf, "r") as f:
			lines = f.readlines()
			for i,line in enumerate(lines):
				if line[0]!="#":
					header_loc = i-1
					break
#			vcf_content = lines[header_loc+1:]
			vcf_record_count = len( lines[header_loc+1:] )
		return vcf_record_count

	def vcf_add_header(self, vcf_ori, content_file):
		cmd = "cat {0} |grep ^\#  | cat - {1} > temp && mv temp {1}".format( vcf_ori, content_file)
#		print( cmd )
		os.system( cmd )





if __name__ == '__main__':

	ob=ob_name()
	#assign variable


