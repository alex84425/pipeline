from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.bwa_aln(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')

		self.args = parser.parse_args(args)

	def bwa_aln(self,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.fastq' |grep R1 ").read().strip().split("\n")
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=10)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
		for file in file_list:
			#cmd = "gunzip "+file

			R1 = file
			R2 = file.replace("R1","R2")
			out_R1 = R1.replace(".","_PE.")
			out_R2 = R2.replace(".","_PE.")
			cmd = "PE_sync {} {} {} {}".format(path+R1, path+R2, output_dir+out_R1, output_dir+out_R2 )
			print(cmd)
			t_list.add_thread( cmd )
		t_list.start_thread()

	
		'''
		for file in file_list:
			eprint("processing file: ",file)
#			cmd= "bwa mem {} {}  {}  -t 30 > {}".format(ref ,path+"/"+file,path+"/"+file.replace("R1","R2"),output_dir+"/"+file.replace("R1","merge").replace("fastq","sam")   )

			R1 = file
			R2 = file.replace("R1","R2")
			out_R1 = R1.replace(".","_PE.")
			out_R2 = R1.replace(".","_PE.")
			cmd = "PE_sync {} {} {} {}".format(path+R1, path+R2, output_dir+out_R1, output_dir+out_R2 )
			print(cmd)	
#			os.system(cmd)
		pass
		'''

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


