from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.co_clean(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					default="co_clean",
					help='input directory')
		""" known site """
		parser.add_argument(	'--KG_hg19_indel' ,type=str, 
					#default="~/ref_seq/dbsnp/GRch37/1000G_phase1.indels.hg19.sites.vcf",
					default="/home/shepherd71c/workplace/neoantigen/CRC_patient_all/genome/1000G_phase1.indels.hg19.sites.vcf",
					help='input 1000G_phase1.indels.hg19.vcf')

		parser.add_argument(	'--M_KG_indel' ,type=str, 
					#default="~/ref_seq/dbsnp/GRch37/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf",
					default="/home/shepherd71c/workplace/neoantigen/CRC_patient_all/genome/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf",
					help='input Mills_and_1000G_gold_standard.indels.hg19.vcf')
		parser.add_argument(	'--dbsnp' ,type=str, 
					#default="~/ref_seq/dbsnp/GRch37/dbsnp_138.hg19.vcf",
					default="/home/shepherd71c/workplace/neoantigen/CRC_patient_all/genome/dbsnp_138.hg19.vcf",
					help='input dbsnp_138.hg19.vcf')

		parser.add_argument(	'--KG_omni' ,type=str, 
					#default="~/ref_seq/dbsnp/GRch37/dbsnp_138.hg19.vcf",
					default="/home/shepherd71c/workplace/neoantigen/CRC_patient_all/genome/1000G_omni2.5.hg19.sites.vcf",
					help='input 1000G_omni2.5')



		parser.add_argument(	'--ref' ,type=str, 
#					default= "genome/95_37.fa",
					help='input_ref')
		parser.add_argument(	'--t' ,type=str, 
					default= 30,
					help='number of threads')
		parser.add_argument(	'--sub_t' ,type=int, 
					default= 2,
					help='number of threads')

		self.args = parser.parse_args(args)

	def co_clean(self,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = glob.glob( "{}/*bam".format(self.args.i_dir) )
		os.system("mkdir -p "+self.args.o_dir)

#		t_list=test_thread.myThread_list(shell_cmd,c=35)
		gatk_path ="~/anaconda3/envs/exome-seq/bin/GenomeAnalysisTK"

		""" check genome_dcit"""
#		print(  ".".join(self.args.ref.split(".")[:-1])+".dict"    )
		if os.path.isfile(  ".".join(self.args.ref.split(".")[:-1])+".dict"   ) == False:
			os.system( "picard CreateSequenceDictionary R={}  O={}".format(self.args.ref, ".".join(self.args.ref.split(".")[:-1])+".dict") )
#

		def shell_cmd(cmd):
			os.system(cmd)

		sub_thread = self.args.sub_t #4
		print("sub thread num:{}".format(sub_thread))
		t_list2=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list3=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list4=test_thread.myThread_list(shell_cmd,c=sub_thread)


		cmd_list = []
		for file in file_list:
			eprint("processing file: ",file)
			intervals = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".intervals")

			
			log1 = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".RealignerTargetCreator.log")
			cmd1 = "{} -T RealignerTargetCreator -R {} -I  {}  -o {} -known {} -known {} -nt {} > {} 2>&1".format(gatk_path, self.args.ref, file, intervals, self.args.KG_hg19_indel, self.args.M_KG_indel, self.args.t, log1)

			log2 = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".IndelRealigner.log")
			ReAlign = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".realigned.bam")
			cmd2 = "{} -T IndelRealigner -R {} -I {} -targetIntervals {}  -known {} -known {} -known {} -o {}  > {} 2>&1".format(gatk_path, self.args.ref, file, intervals, self.args.KG_hg19_indel, self.args.M_KG_indel, self.args.KG_omni,ReAlign,  log2 )
#			os.system("mv *realigned* {}".format( self.args.o_dir ))
#			print(cmd)

			log3 = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".BaseRecalibrator.log")
			grp_file= self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".recal.grp")
			cmd3 =("{} -T BaseRecalibrator -R {} -I {} -knownSites {} -knownSites {} -knownSites {} -knownSites {} -o {}  > {} 2>&1"
				.format(gatk_path, self.args.ref, ReAlign, self.args.dbsnp, self.args.KG_hg19_indel, self.args.M_KG_indel, self.args.KG_omni, grp_file, log3))


#			print(cmd3)


#			printread_output = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".post_recal.grp")
#			cmd4_skipmelo = "{} -T BaseRecalibrator -R {} -I {} -BQSR {}  -knownSites {} -knownSites {} -knownSites {} -o  {}".format(gatk_path, self.args.ref, ReAlign, grp_file, self.args.dbsnp, self.args.KG_hg19_indel, self.args.M_KG_indel, printread_output )

			log4 = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".PrintReads.log")
			printread_output = self.args.o_dir+"/"+file.split("/")[-1].replace(".bam",".printread.bam")
			cmd4 = ("{} -T PrintReads -R {} -I {} -BQSR {}  -o {}  > {} 2>&1 ".format(gatk_path, self.args.ref, ReAlign, grp_file, printread_output,  log4))
#			print( cmd1 )
			os.system( cmd1 )

			t_list2.add_thread( cmd2 )
			t_list3.add_thread( cmd3 )
			t_list4.add_thread( cmd4 )
#			print( cmd2 )
#			os.system( cmd2 )
#			print( cmd3 )
#			os.system( cmd3 )
#			print( cmd4 )
#			os.system( cmd4 )
#			os.system( cmd3 )
#			print( cmd2 )
#			print( cmd3 )
#			print( cmd4 )
#			os.system( cmd3 )
#			os.system( cmd4 )

#			cmd_list.append( "{};{};{};".format(cmd2,cmd3,cmd4) )
		t_list2.start_thread()
		t_list3.start_thread()
		t_list4.start_thread()




#			os.system(cmd)	
#			t_list.add_thread( cmd )
#		t_list.start_thread()
		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


