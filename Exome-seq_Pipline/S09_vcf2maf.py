from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.to_threads(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					default = "",
					help='output directory')
		parser.add_argument(	'--ass' ,type=str, 
					default=" GRCh37 ",
					help='--ncbi-build')
		parser.add_argument(	'--ref' ,type=str, 
					default= "~/.vep/homo_sapiens/95_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz",
					help='input fa or fa.gz')
		parser.add_argument(	'--t' ,type=int, 
					default= 8,
					help='file keyword')

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str,
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		'''
		parser.add_argument(	'--p' ,type=str, 
					help='input picard.jar')
		'''
		self.args = parser.parse_args(args)

	def to_threads(self,ref,path, output_dir):

#		file_list =  glob.glob("{}/{}".format(self.args.i_dir, self.args.key ))

		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )


		if self.args.o_dir =="":
			if self.args.i_dir[-1] !="/":
				self.args.o_dir = self.args.o_dir + "/"
			
			self.args.o_dir =  "." + ("/".join(self.args.i_dir.split("/")[:-2]) )+ "/" + "maf_" + self.args.i_dir.split("/")[-2]

		print("output dir: ", self.args.o_dir )
		os.system("mkdir -p "+self.args.o_dir)

		#check reference
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=self.args.t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 0

		for i,file in enumerate(file_list):
#			ab_path = os.path.abspath(self.args.i_dir)
			local_file = file.split("/")[-1]
#			print(file)
#			print(ab_path)
			ref = self.args.ref
			#vep_path= "~/anaconda3/envs/exome-seq/bin/"
			vep_path= "~/miniconda*/envs/exome-seq/bin/"

			fork_num = self.args.t
			tumor_id = file.split("/")[-1].split(".")[0]
			normal_id = file.split("/")[-1].split(".")[0]
			cmd ="vcf2maf.pl --input-vcf {} --output-maf  {} --ref-fasta   {} --vep-path {}  --vep-forks {}	--tumor-id {} --normal-id {} --ncbi-build {}".format(file, self.args.o_dir+"/"+local_file+".maf",ref,vep_path ,fork_num, tumor_id, normal_id,self.args.ass )

			#001.vcf.snp.Somatic.vep.vcf
			tmp_vep_file = file+".vep.vcf"

			remove_old_file =   "rm {};".format(tmp_vep_file) + cmd 
			#cmd = remove_old_file+"\n"+cmd	
#			os.system(  "rm {};".format(tmp_vep_file) + cmd )

			print( cmd )
#			os.system( cmd )
			t_list.add_thread( cmd )
		t_list.start_thread()

		os.system("  rm {}/{}".format(self.args.i_dir, "*vep*"))

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


