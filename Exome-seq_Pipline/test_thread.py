import threading
import sys
import os
import time

def eprint(*args, **kwargs):
	print( *args, file=sys.stderr, **kwargs  )


class myThread (threading.Thread):
	def __init__(self,target,*args):
		threading.Thread.__init__(self)
		self._target=target
		self._args= args
	def run(self):
		self._target(*self._args)


class myThread_list:
		
	def __init__(self,target,c=40):
		self.core_num=c
		self._target=target	
		self.thread_list=[]

		self.sleep_time=0

	def give_core(self,core_num):
		self.core_num=core_num
	def sleep_time(self,time):
		self.sleep_time=time

		

	def add_thread(self,*args):
		tmp_t=myThread( self._target,*args )
		self.thread_list.append(tmp_t)

	def print_list(self):
		print( len(self.thread_list))

	def start_thread(self):
		for i in range(0, len(self.thread_list), self.core_num) :
			if i%self.core_num==0:
				sub_thread_list=self.thread_list[i:i+self.core_num]
			else:
				sub_thread_list=self.thread_list[i:]

			for ele in sub_thread_list:
				ele.start()
			for ele in sub_thread_list:
				ele.join()
			eprint("pool ",i,"")
			if self.sleep_time!=0:
				eprint( "sleep: ",self.sleep_time)
				time.sleep(  self.sleep_time)
		

		

def print_str( i ):
	print( str(i) )

# Create new threads
if __name__=="__main__":
	t_list=myThread_list(print_str)

	t_list.sleep_time=2
	for i in range(100):
		t_list.add_thread(i)
	t_list.print_list()
	t_list.start_thread()
'''
t_list=[]
for i in range(10):
	#t_list.append(myThread(1, "Thread-"+str(i), i ) )
	t_list.append(myThread( print_str,i) )

for ele in t_list:
	ele.start()

for ele in t_list:
	ele.join()
'''
# Start new Threads
