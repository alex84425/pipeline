from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO


def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.varscan_cmd(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		self.Spilit_Somatic_Gerline()
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ../S08_varscan.py  --i_dir MarkDuplicate/  --o_dir varscan_file/ --ref ../genome/genome_NCBI_37.fa --p ../../exome_seq/picard/build/libs/picard.jar')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--p' ,type=str, 
					help='input picard.jar')
		parser.add_argument(	'--ref' ,type=str, 
					help='input reference.fa')

		parser.add_argument(	'--kw' ,type=str, 
					default= "*N*bam",
					help='file substring')

		self.args = parser.parse_args(args)

	def varscan_cmd(self,ref,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' |grep -v bai |grep N").read().strip().split("\n")
		file_list = [ ele.split("/")[-1] for ele in glob.glob( path+"/"+self.args.kw) ]
		

		print("file_list",file_list)
		os.system("mkdir -p "+self.args.o_dir)

		# thread method
		t_list=test_thread.myThread_list(shell_cmd,c=35)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 1
		for file in file_list:
			normal_bam = path+"/"+file
			tumor_bam =  path+"/"+file.replace("N","T")

			N_output_pipeup =self.args.o_dir+"/"+ file[:4]+".pipeup"
			T_output_pipeup =self.args.o_dir+"/"+ file[:4].replace("N","T")+".pipeup"
			#pipeup_cmd = ("samtools mpileup -f {} -q 1 -B {} {} >  {} ;".format(self.args.ref, normal_bam, tumor_bam, output_pipeup ) )
			N_pipeup_cmd = ("samtools mpileup -f {} {}> {};".format(self.args.ref, normal_bam, N_output_pipeup ) )
			T_pipeup_cmd = ("samtools mpileup -f {} {}> {};".format(self.args.ref, tumor_bam, T_output_pipeup ) )
			print(  N_pipeup_cmd )
			print(  T_pipeup_cmd )
#			print(pipeup_cmd)
			N_input_pipeup = N_output_pipeup
			T_input_pipeup = T_output_pipeup
			output_vcf = self.args.o_dir+"/"+file[:3]+".vcf"
			somatic_cmd = "varscan  somatic {}  {} {} --mpileup 1 --min-coverage 8 --min-coverage-normal 8 --min-coverage-tumor 6 --min-var-freq 0.10 --min-freq-for-hom 0.75 --normal-purity 1.0 --tumor-purity 1.00 --p-value 0.99 --somatic-p-value 0.05 --strand-filter 0 --output-vcf".format(N_input_pipeup, T_input_pipeup, output_vcf)
#			print( somatic_cmd)
			cmd_process_snp   = ("varscan  processSomatic {}  --min-tumor-freq 0.10 --max-normal-freq 0.05 --p-value 0.07").format(file[:3]+".vcf.snp" )
			cmd_process_indel = ("varscan processSomatic {}  --min-tumor-freq 0.10 --max-normal-freq 0.05 --p-value 0.07").format(file[:3]+".vcf.indel" )
		
#			t_list.add_thread( somatic_cmd )
#			print( somatic_cmd )
#			t_list.add_thread( cmd_process_snp )
#			t_list.add_thread( cmd_process_indel )
			t_list.add_thread( "{}\n{}\n{}\n{}\n{}\n{}\n".format(N_pipeup_cmd, T_pipeup_cmd ,somatic_cmd,"cd "+self.args.o_dir ,cmd_process_snp, cmd_process_indel )  )


			'''
			output_copynumber = self.args.o_dir+"/"+file[:3]
			CN_cmd = "java -jar {}  copynumber  {} {} --mpileup 1 ;".format("../varscan/VarScan.v2.4.0.jar ", input_pipeup, output_copynumber)

			#java -jar VarScan.jar copyCaller varScan.copynumber --output-file varScan.copynumber.called
			CNVcall_cmd = "java -jar {}  copyCaller  {} --output-file {} ;".format("../varscan/VarScan.v2.4.0.jar ",output_copynumber+".copynumber" , output_copynumber+".copynumber.called " )

#			print(CN_cmd)
#			print(CNVcall_cmd)
#			t_list.add_thread(  CN_cmd +  " \n " + CNVcall_cmd )
#			print(CNVcall_cmd)
			'''
			
		t_list.start_thread()

		"""merge vcf (only LOH and Somatic) """
		'''
		for file in file_list:
			output_vcf = self.args.o_dir+"/"+file[:3]+".vcf"


			"""merge vcf """
			os.system( "rm  {}".format(output_vcf)  )
			os.system( "cat  {}/{}*.Somatic |grep -v ^# >> {}".format(self.args.o_dir,file[:3] , output_vcf)  )
			os.system( "cat  {}/{}*.LOH |grep -v ^# >> {}".format(self.args.o_dir,file[:3] , output_vcf)  )
		'''

	def Spilit_Somatic_Gerline(self):
		Somatic_file_list = sorted(glob.glob("{}/*.vcf.*.Somatic".format(self.args.o_dir)))
#		Gerline_file_list = glob.glob("{}/*.vcf.*.Gerline".format(self.args.o_dir))
#		print( [ file for file in Somatic_file_list if "snp" in file])
#		print( [ file for file in Somatic_file_list if "indel" in file])

		for file_group in zip([ file for file in Somatic_file_list if "indel" in file],[ file for file in Somatic_file_list if "snp" in file] ):
			indel_file = file_group[0]
			snp_file = file_group[1]
#			print( file_group)
			output = file_group[0].replace(".indel.",".snp_indel.")
			os.system( " cat {} > {}  ".format( indel_file, output ) )
			os.system( " cat {} >> {} ".format( snp_file, output ) )



		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


