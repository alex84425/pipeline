from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.qc_and_trimmer_all(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')

		self.args = parser.parse_args(args)

	def qc_and_trimmer_all(self,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.fastq' ").read().strip().split("\n")
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=10)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
		for file in file_list:
			#cmd = "gunzip "+file
			cmd = ("cat "+   path+"/"+file +"  | fastq_quality_trimmer -t 20 -l 53 -Q33 "+
	  	          " | fastq_quality_filter -q 30 -p 70 -Q33 "+
		          " > "+ output_dir+"/"+file  )	
			print(cmd)
			t_list.add_thread( cmd )
		t_list.start_thread()

		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


