"""
useful document: 

(1) http://www.zxzyl.com/archives/996
(2) https://samtools.github.io/bcftools/howtos/filtering.html
conda install -c bioconda samtools openssl=1.0

bcftools query -f '[%POS %SAMPLE DP:%DP AD:%AD\n]'     -i 'FORMAT/AD[1:1]/FORMAT/DP[1]>0.05 && FORMAT/AD[0:1]/FORMAT/DP[0]<0.01' GATK_Mutect2_412_selected/050T.recal.snp.vcf 
"""
from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


def vcf_counter(in_f):
	with open(in_f, 'r') as f :
		line = [ ele for ele in f.readlines() if ele[0] != '#']
#		print( "{}: {}".format(file, len(line)))
		return len(line)

class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')
		parser.add_argument(	'--key' ,type=str, 
					default = "*vcf*",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=8,
					help='input directory')

		parser.add_argument(	'--f' ,type=str, 
					default = 'QSS>100 && QSS_NT>100 && SomaticEVS > 10 && MQ>55 && INFO/DP>90',
					help='select/exclude sites for which the expression is true ')
		parser.add_argument(	'--f_log' ,type=str, 
					default = '',
					help='log of select/exclude sites for which the expression is true ')

		parser.add_argument(	'--f_f' ,type=int,
					default=0,
					help="ex: first 30 file")


		self.args = parser.parse_args(args)

	def main(self,path, output_dir):
		""" bcftools view -i 'QSS>100 && QSS_NT>100 && SomaticEVS > 10 && MQ>55 && INFO/DP>90' -f PASS Strelka_T1R1_vs_Ctrl_somatic_snvs.vcf.gz """
		""" gzip -c  strelka_file/001.somatic.snvs.vcf  | bcftools view  -i 'QSS>100 && QSS_NT>100 && SomaticEVS > 10 && MQ>55 && INFO/DP>90' -f PASS  |  grep -v ^# """
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )))
		if self.args.f_f!=0:
			file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )[:self.args.f_f]

		
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)


		if self.args.f_log == "":
			log = self.args.o_dir +"/" +"filter_para.txt"
		else :
			os.system("rm "+ self.args.o_dir +"/" + self.args.f_log)
		t_list.sleep_time = 2

		total_count = 0
		total_pass_count = 0
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			output = self.args.o_dir + "/" + f_n
			filter_para = self.args.f
			#cmd = "gzip -c {}  | bcftools view  -i '{}' -f PASS   > {}".format(file, filter_para, output )

			if filter_para =="":
				cmd = " bcftools view  {}  -f PASS   > {}".format(file, output )
			else:
				if "GATK" in file:
					cmd = " bcftools view  {} -i '{}'   > {}".format(file, filter_para, output )
				else:
					cmd = " bcftools view  {} -i '{}' -f PASS   > {}".format(file, filter_para, output )
			with open(log, "w") as f:
				f.write( "{}\n".format( output)  )

#			os.system( cmd)
			t_list.add_thread( cmd )
		t_list.start_thread()
		""" see count diff"""
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			output = self.args.o_dir + "/" + f_n
			t_list.add_thread( cmd )

#			verbose =  "{} -> {} : {} -> {} ".format(f_n, output.split("/")[-1], vcf_counter(file), vcf_counter(output) ) 
#			print( verbose )
			count = vcf_counter( file )
			pass_count = vcf_counter( output )
			total_count +=  count
			total_pass_count +=  pass_count
			verbose = ( "{}: {} -> {}, ({}%)".format(f_n, count, pass_count, round(100*pass_count/count,2 ) ) )
			print( verbose )
			with open(log, "a") as f:
				f.write( "{}\n".format( ( verbose ) ) )

		print( "total: {} -> {}, ({}%)".format(total_count, total_pass_count, round(100*total_pass_count/total_count,2 ) ) )
		print( "avg: {} -> {}, ({}%)".format(
										  round( total_count/len(file_list),2 )
										, round( total_pass_count/len(file_list),2 )
										, round(100*total_pass_count/total_count,2 ) ) )
		""" log """
		with open(log, "a") as f:
			f.write( "{}\n".format( self.args.f)  )


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


