from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.sam_to_bam(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--ref' ,type=str, 
					help='input directory')
		parser.add_argument(	'--p' ,type=str, 
					help='input picard.jar'
					,default="")
		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='sub thread')
		""" filter seletor """
		parser.add_argument(	'--key' ,type=str,
					default = "*sam",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')
		


		self.args = parser.parse_args(args)

	def sam_to_bam(self,ref,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.sam' ").read().strip().split("\n")
#		file_list = glob.glob("{}/*sam".format(path) )

		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )



		os.system("mkdir -p "+self.args.o_dir)

		#check reference
		from pathlib import Path
		fai_path = Path(self.args.ref+".fai")
		if (fai_path.is_file())==False:
			os.system("	samtools faidx {}".format(self.args.ref))


		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=self.args.sub_t)
		for file in file_list:
			eprint("processing file: ",file)
			file_name = file.split("/")[-1]
			#cmd = "java -jar {} SortSam   INPUT={}   OUTPUT={}   SORT_ORDER=coordinate".format( self.args.p,path+"/"+file ,self.args.o_dir+"/"+file.replace(".sam",".bam"))
			cmd = "picard SortSam   INPUT={}   OUTPUT={}   SORT_ORDER=coordinate".format( file ,self.args.o_dir+"/"+file_name.replace(".sam",".bam"))

#			print(cmd)	
#			os.system(cmd)
			t_list.add_thread( cmd )
		t_list.start_thread()

		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


