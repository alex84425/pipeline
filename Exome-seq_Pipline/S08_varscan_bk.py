from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
#from Bio import SeqIO


def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.varscan_cmd(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ../S08_varscan.py  --i_dir MarkDuplicate/  --o_dir varscan_file/ --ref ../genome/genome_NCBI_37.fa --p ../../exome_seq/picard/build/libs/picard.jar')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--p' ,type=str, 
					help='input picard.jar')
		parser.add_argument(	'--ref' ,type=str, 
					help='input reference.fa')

		self.args = parser.parse_args(args)

	def varscan_cmd(self,ref,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.bam' |grep -v bai |grep N").read().strip().split("\n")
		os.system("mkdir -p "+self.args.o_dir)

		# thread method
		t_list=test_thread.myThread_list(shell_cmd,c=4)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 1
		for file in file_list:
			normal_bam = path+"/"+file
			tumor_bam =  path+"/"+file.replace("N","T")
			output_pipeup =self.args.o_dir+"/"+ file[:3]+".pipeup"
			pipeup_cmd = ("samtools mpileup -f {} -q 1 -B {} {} >  {} ;"
						.format(self.args.ref, normal_bam, tumor_bam, output_pipeup ) )
#			print(pipeup_cmd)
			input_pipeup = output_pipeup
			output_vcf = self.args.o_dir+"/"+file[:3]+".vcf"
			somatic_cmd = "java -jar {}  somatic {}  {} --mpileup 1 --min-coverage 8 --min-coverage-normal 8 --min-coverage-tumor 6 --min-var-freq 0.10 --min-freq-for-hom 0.75 --normal-purity 1.0 --tumor-purity 1.00 --p-value 0.99 --somatic-p-value 0.05 --strand-filter 0 --output-vcf".format("../varscan/VarScan.v2.4.0.jar ", input_pipeup, output_vcf)
#			print( somatic_cmd)
			cmd_process_snp = "cd "+self.args.o_dir+" ;"+("java -jar ../../varscan/VarScan.v2.4.3.jar processSomatic {}  --min-tumor-freq 0.10 --max-normal-freq 0.05 --p-value 0.07").format(file[:3]+".vcf.snp" )
			cmd_process_indel = "cd "+self.args.o_dir+" ;"+("java -jar ../../varscan/VarScan.v2.4.3.jar processSomatic {}  --min-tumor-freq 0.10 --max-normal-freq 0.05 --p-value 0.07").format(file[:3]+".vcf.indel" )
#			print( cmd_process)
		
#			t_list.add_thread( somatic_cmd )
#			t_list.add_thread( cmd_process_snp )
#			t_list.add_thread( cmd_process_indel )


			output_copynumber = self.args.o_dir+"/"+file[:3]
			CN_cmd = "java -jar {}  copynumber  {} {} --mpileup 1 ;".format("../varscan/VarScan.v2.4.0.jar ", input_pipeup, output_copynumber)

			#java -jar VarScan.jar copyCaller varScan.copynumber --output-file varScan.copynumber.called
			CNVcall_cmd = "java -jar {}  copyCaller  {} --output-file {} ;".format("../varscan/VarScan.v2.4.0.jar ",output_copynumber+".copynumber" , output_copynumber+".copynumber.called " )

#			print(CN_cmd)
#			print(CNVcall_cmd)
			t_list.add_thread(  CN_cmd +  " \n " + CNVcall_cmd )
#			print(CNVcall_cmd)
			
			""" """


		t_list.start_thread()


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


