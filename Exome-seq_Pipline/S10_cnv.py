from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
import pandas as pd
import numpy as np
from pyensembl import EnsemblRelease
from sklearn.metrics import confusion_matrix


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
#		self.sam_to_bam(self.args.ref,self.args.i_dir,self.args.o_dir)
		self.read_file()
		self.output_csv()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python S10_cnv.py --i_dir CT18003/varscan_file/ --csv copynum_type.csv')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'-*csv' ,type=str, 
					help='output csv')

		self.args = parser.parse_args(args)

	def read_file(self):
		self.data_list=[]
		self.file_name_list = []
		self.gene_list = []

		self.file_list=  glob.glob("{}/*copynumber.called".format(self.args.i_dir )) 
		data = EnsemblRelease(75) # will take a while to get the genome data installed in your system
		for i,file in enumerate(self.file_list):
			file_name =  file.split("/")[-1]

			self.file_name_list.append(file_name)
			df = pd.read_csv(file,sep='\t',skiprows=0)
#			print(df.columns)
			if len(df.index) ==0:
				continue

			df['gene'] = df.apply(lambda row: data.gene_names_at_locus(row['chrom'], row['chr_start'])[0], axis=1)

			copy_type = df['region_call'].value_counts().idxmax()
			gene = df['gene'].value_counts().idxmax()
			self.gene_list.append( gene )
			#self.data_list.append( "{}\t{}\t{}".format( file_name ,gene , copy_type ) )
			self.data_list.append( [ file_name ,gene , copy_type ] )
#		print(self.data_list)	
		self.gene_list= list(set(self.gene_list))

	def output_csv(self):

		td_arr =  [[ "" for ele in range(len(self.file_name_list ))  ]  for ele in range(len(self.gene_list ))    ]
		df = pd.DataFrame(data=td_arr ,    # values
              index=self.gene_list,    # 1st column as index
              columns= self.file_name_list)  

		for ele in self.data_list:
			df.set_value(ele[1], ele[0] ,ele[2])
		
		df['gene_syb'] = df.index
		cols =  [df.columns[-1]] + list( df.columns[:-1])
		df = df[  cols ]
		df.to_csv(self.args.csv,sep="\t",index=False)
		print( df)


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


