from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.sam_to_bam(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--ref' ,type=str, 
					help='input directory')
		parser.add_argument(	'--p' ,type=str, 
					help='input picard.jar')

		self.args = parser.parse_args(args)

	def sam_to_bam(self,ref,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.vcf' |grep -v vep").read().strip().split("\n")
#		os.system("mkdir -p "+self.args.o_dir)

		#check reference
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=1)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 0
		for i,file in enumerate(file_list):


			ab_path = os.path.abspath(self.args.i_dir)
			
#			print(file)
#			print(ab_path)
			cmd ="docker exec vep_bg  vep -i  /opt/vep/.vep/{}  -o  /opt/vep/.vep/{}  --port 3337 --format vcf --force_overwrite --cache --force_overwrite --sift b --canonical --symbol  --tab --fields Uploaded_variation,SYMBOL,CANONICAL,SIFT --fork 40".format(file,file+".vepout")
#			cmd ="docker run   -id -v {}:/opt/vep/.vep ensemblorg/ensembl-vep /bin/sh -c \"vep -i  /opt/vep/.vep/{}  -o  /opt/vep/.vep/{}  --port 3337 --format vcf --force_overwrite --cache --force_overwrite --sift b --canonical --symbol  --tab --fields Uploaded_variation,SYMBOL,CANONICAL,SIFT\"".format(ab_path,file,file+".vepout")
			print( cmd )
			t_list.add_thread( cmd )
			
		
		t_list.start_thread()


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


