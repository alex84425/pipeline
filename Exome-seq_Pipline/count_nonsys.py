from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ~/pipeline/Exome-seq_Pipline/count_nonsys.py --i merge_filter.maf')

	
		parser.add_argument(	'--i' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o' ,type=str, 
					default="",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')

		parser.add_argument(	'--rename_barcode' ,type=str, 
					default="{}",
					help="example: CRC{}_PDX")

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')

		
		parser.add_argument(	'--f_f' ,type=int, 
					default = 0,
					help="ex: first 30 file")

		self.args = parser.parse_args(args)

	def main(self):
		df = self.read_maf( self.args.i )
		TB_list = list(set(list(df["Tumor_Sample_Barcode"])))
		nonsy_snv_df = self.nonsy_snv(df)
		nonsy_indel_df = self.nonsy_indel(df)
#		print( nonsy_snv_df )
#		nonsy_df[""]
		print("total non-syn snv:", len(nonsy_snv_df))
		print("total non-syn indel:", len(nonsy_indel_df))
		print("avg non-syn snv:", round(len(nonsy_snv_df)/ len(TB_list),2 ))
		print("avg non-syn indel:", round(len(nonsy_indel_df)/ len(TB_list), 2 ))

		nonsy_snv_n_df = self.nonsy_snv(df[df['dbSNP_RS'] =="novel"])
		nonsy_indel_n_df = self.nonsy_indel(df[df['dbSNP_RS'] =="novel"])
		print("filter mutation in dbSNP:")
		print("total novel non-syn snv:", len(nonsy_snv_n_df))
		print("total novel non-syn indel:", len(nonsy_indel_n_df))
		print("avg novel non-syn snv:", round(len(nonsy_snv_n_df)/ len(TB_list),2 ))
		print("avg novel non-syn indel:", round(len(nonsy_indel_n_df)/ len(TB_list), 2 ))



		
	def group_by_gene(self, merge_df, output, g_col):
		TB_list = list(set(list(merge_df["Tumor_Sample_Barcode"])))
		for i,TB in enumerate( TB_list ) :

			df = merge_df[merge_df[ "Tumor_Sample_Barcode"] ==TB]

			g_df = df.groupby( by  = g_col, as_index=True)["Variant_Classification"].count().to_frame()

#			g_df.sort_values(by=[ "Variant_Classification"  ], inplace = True, ascending = False)
			df = g_df	
			df = df.reset_index()
			col = TB

			merge_col = ":".join(g_col)
			df[col] = df["Variant_Classification"]
			if len(g_col)>=2:
				df[ merge_col ] = df[g_col[0]]  + df[g_col[1]].astype( dtype="str" )
			df = df [ [merge_col,col]] 


			if i == 0:		
				gene_df = df		
			else: 
				gene_df = pd.merge(gene_df, df, left_on = merge_col, right_on = merge_col, how= "outer" )

		""" count to 1 and sum and average """
		print( gene_df.columns)
		gene_df["Sum"] = gene_df.iloc[:,1:].sum(axis = 1)
		gene_df["Avg"] = gene_df["Sum"] / len( TB_list)
		gene_df.sort_values(by=[ "Sum"  ], inplace = True, ascending = False)

		""" output groupby gene """

		if len(g_col)>=2:	
			g_output = output.replace(".maf","_GroupByPos.maf")
		else:
			g_output = output.replace(".maf","_GroupByGene.maf")
		gene_df.to_csv(g_output, sep="\t", index=False)

	

	def split_list(self, l):
		ID_list = [ ele.split("/")[-1][:3] for ele in l]
		ID_list = list(set(ID_list))
		td_l = []
		for ID in ID_list:
			g = [ ele  for ele in l if ID in ele ]

			td_l.append( g )

		return td_l


	
	def nonsy_snv(self, df):
		v_col =[ 
			  'Missense_Mutation',
			  'Nonsense_Mutation'
		]
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df

	def nonsy_indel(self, df):
		v_col =[ 
			  'Frame_Shift_Del',
			  'Frame_Shift_Ins',
			  'In_Frame_Ins',
			  'Splice_Site',
			  'In_Frame_Del']
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df


	def filter_maf(self, df):
		v_col =[ 'Frame_Shift_Del',
			  'Missense_Mutation',
			  'Nonsense_Mutation',
			  'Multi_Hit',
			  'Frame_Shift_Ins',
			  'In_Frame_Ins',
			  'Splice_Site',
			  'In_Frame_Del']
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df
	def padding_barcode(self, df):
		df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].astype(dtype = "str").apply( lambda x: x.zfill(3 )  )
		return df



	def rename_barcode(self, df):

		#df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].apply( lambda x: "CRC{}_PDX".format(x) )
		df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].apply( lambda x: self.args.rename_barcode.format(x) )
		print(df["Tumor_Sample_Barcode"] )

		return df


	def read_maf(self, maf):
		maf_df = pd.read_csv(maf, sep = "\t", comment="#")
#		print( maf_df )
		return maf_df
#		input()

		pass
""" 
Hugo_Symbol Entrez_Gene_Id  Center  NCBI_Build  Chromosome  Start_Position  End_Position	Strand  Variant_Classification  Variant_Type	Reference_Allele	Tumor_Seq_Allele1   Tumor_Seq_Allele2   dbSNP_RS	dbSNP_Val_Status	Tumor_Sample_Barcode	Matched_Norm_Sample_Barcode Match_Norm_Seq_Allele1  Match_Norm_Seq_Allele2  Tumor_Validation_Allele1	Tumor_Validation_Allele2	Match_Norm_Validation_Allele1   Match_Norm_Validation_Allele2   Verification_Status Validation_Status   Mutation_Status Sequencing_Phase	Sequence_Source Validation_Method   Score   BAM_File	Sequencer   Tumor_Sample_UUID   Matched_Norm_Sample_UUID	HGVSc   HGVSp   HGVSp_Short Transcript_ID   Exon_Number t_depth t_ref_count t_alt_count n_depth n_ref_count n_alt_count all_effects Allele  Gene	Feature Feature_type	Consequence cDNA_position   CDS_position	Protein_position	Amino_acids Codons  Existing_variation  ALLELE_NUM  DISTANCE	STRAND_VEP  SYMBOL  SYMBOL_SOURCE   HGNC_ID BIOTYPE CANONICAL   CCDS	ENSP	SWISSPROT   TREMBL  UNIPARC RefSeq  SIFT	PolyPhen	EXON	INTRON  DOMAINS AF  AFR_AF  AMR_AF  ASN_AF  EAS_AF  EUR_AF  SAS_AF  AA_AF   EA_AF   CLIN_SIG	SOMATIC PUBMED  MOTIF_NAME  MOTIF_POS   HIGH_INF_POS	MOTIF_SCORE_CHANGE  IMPACT  PICK	VARIANT_CLASS   TSL HGVS_OFFSET PHENO   MINIMISED   ExAC_AF ExAC_AF_AFR ExAC_AF_AMR ExAC_AF_EAS ExAC_AF_FIN ExAC_AF_NFE ExAC_AF_OTH ExAC_AF_SAS GENE_PHENO  FILTER  flanking_bps	vcf_id  vcf_qual	ExAC_AF_Adj ExAC_AC_AN_Adj  ExAC_AC_AN  ExAC_AC_AN_AFR  ExAC_AC_AN_AMR  ExAC_AC_AN_EAS  ExAC_AC_AN_FIN  ExAC_AC_AN_NFE  ExAC_AC_AN_OTH  ExAC_AC_AN_SAS  ExAC_FILTER gnomAD_AF   gnomAD_AFR_AF   gnomAD_AMR_AF   gnomAD_ASJ_AF   gnomAD_EAS_AF   gnomAD_FIN_AF   gnomAD_NFE_AF   gnomAD_OTH_AF   gnomAD_SAS_AF   vcf_pos

"""

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


