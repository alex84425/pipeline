from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.sam_to_bam(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')


		self.args = parser.parse_args(args)

	def sam_to_bam(self,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
		for i,file in enumerate(file_list):
			file_out = file.replace(".bam","_AddReadGroup.bam")
			G_ID = file.split(".")[0]
			#cmd ="java -jar {}  AddOrReplaceReadGroups I={} O={}  RGID={} RGLB=lib1 RGPL=illumina RGPU={}  RGSM={} ".format( self.args.p, path+"/"+file ,self.args.o_dir+"/"+file_out, G_ID ,G_ID,G_ID )

			cmd ="picard  AddOrReplaceReadGroups I={} O={}  RGID={} RGLB=lib1 RGPL=illumina RGPU={}  RGSM={} ".format(  path+"/"+file ,self.args.o_dir+"/"+file_out, G_ID ,G_ID,G_ID )
			t_list.add_thread( cmd )
			
		
		t_list.start_thread()

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


