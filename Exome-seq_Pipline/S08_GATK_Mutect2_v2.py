"""
tool index: https://gatk.broadinstitute.org/hc/en-us/categories/360002369672
"""

from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll

def fetch_tag(bam):
	#@RG ID:003N_merge_001
	lines = os.popen("samtools view -H {}".format( bam )  ).read().split("\n")
#	tag  = [ele for ele in lines if "merge" in ele ][0].split("\t")[1][3:]
	tag  = [ele for ele in lines if "SM:"  in ele ][0].split("\t")[1].split(":")[1]
	return tag

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])

		if self.args.c_p :
			self.create_normal_panel()
			pass
		else:
			self.mutect2()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description=
		'python ~/ExomeSeq_Src/Exome_Pipeline/S08_GATK_Mutect2.py --i_dir GATK_recal/   --key "*T.recal.bam" --unify_n  GATK_somatic_calling_StartupCmd/C1.recal.bam --n_t C1 --exon_bed region_bed/S07604514_AllTracks.bed  --ref genome/ucsc.hg19.fasta --n_p GATK_somatic_calling_StartupCmd/normal.vcf.gz'

		
		""" 
		create normal panel
		python ~/pipeline//Exome-seq_Pipline/S08_GATK_Mutect2_v2.py --i_dir GATK_recal/   --key "*T.recal.bam"  --exon_bed region_bed/S07604514_AllTracks.bed  --ref genome/ucsc.hg19.fasta --n_p "" --o_dir GATK_Mutect2_PoN --sub_t 10 --c_p
		"""
		)
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					default="GATK_Mutect2",
					help='input directory')

#		parser.add_argument(	'--dbsnp' ,type=str, 
#					default="~/ref_seq/dbsnp/GRch37/dbsnp_138.hg19.vcf",
#					help='input dbsnp_138.hg19.vcf')

		parser.add_argument(	'--ref' ,type=str, 
#					default= "genome/95_37.fa",
					help='input_ref')
		parser.add_argument(	'--t' ,type = int, 
					default= 5,
					help='number of threads')
		parser.add_argument(	'--sub_t' ,type=int, 
					default= 5,
					help='number of threads')

		parser.add_argument(	'--n_p' ,type=str, 
					default = "",
					help= "normal _panel")
		parser.add_argument(	'--unify_n' ,type=str, 
					default= "",
					help='unify normal if you need')
		'''
		parser.add_argument(	'--n_tag' ,type=str, 
					default= "",
					help='sample name of unify normal bam')
		'''

		parser.add_argument(	'--exon_bed' ,type=str, 
					default= "region_bed/S07604514_AllTracks.bed",
					help='input exon bed')
		parser.add_argument(	'--c_p' ,action="store_true", 
					default= "",
					help='mode: create panel')

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str,
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')



		self.args = parser.parse_args(args)

	def create_normal_panel(self):

		""" https://gatk.broadinstitute.org/hc/en-us/articles/360035531132 """
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )
		os.system("mkdir -p "+self.args.o_dir)
#		t_list=test_thread.myThread_list(shell_cmd,c=35)
#		gatk_path ="~/anaconda3/envs/exome-seq/bin/GenomeAnalysisTK"


		def shell_cmd(cmd):
			os.system(cmd)

		sub_thread = self.args.sub_t #4
		print("sub thread num:{}".format(sub_thread))
		t_list2=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list3=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list4=test_thread.myThread_list(shell_cmd,c=sub_thread)

		""" check build reference dict"""
		if os.path.isfile( ".".join( self.args.ref.split(".")[:-1] ) + ".dict" ) ==False:
			cmd = "picard CreateSequenceDictionary  R={}  O={}".format(self.args.ref, ".".join( self.args.ref.split(".")[:-1] ) + ".dict")

		""" check bai exit"""
		if os.path.isfile( file_list[0].replace(".bam",".bai") ) ==False:
			all_file = [ ele.repalce("T", "N") for ele in file_list] + file_list 
			for file in all_file:
				cmd = ( "samtools index {} {} ".format(file, file.replace(".bam",".bai")) )
				os.system( cmd )



		cmd_list = []
		vcf_list = []
		for file in sorted(file_list ):
			eprint("processing file: ",file)

			tumor_bam = file
			tumor_tag =  fetch_tag(tumor_bam) 
			""" check normal bam"""
			if self.args.unify_n != "":
				normal_bam = self.args.unify_n
			else:
				file_item = file.split("/")
				normal_bam =  "/".join( file_item[:-1] ) + "/" + file_item[-1].replace("T","N")
				
#			print("normal_bam: ",normal_bam)
			""" check normal bam tag"""
#			if self.args.n_tag != "":
#				normal_tag =  fetch_tag( normal_bam) 
#			else:
			normal_tag =  fetch_tag( normal_bam) 
			
			""" main cmd """
			
			if True:
				"""step 01: run Mutect2 in tumor-only mode on each normal sample """
				output1 = self.args.o_dir + "/" + normal_bam.split("/")[-1].replace(".bam", ".vcf.gz")
				vcf_list.append( output1 )
				log1	= self.args.o_dir + "/" + normal_bam.split("/")[-1].replace(".bam", ".vcf.gz.log")
						
				cmd1= ("gatk Mutect2 -I {} -tumor {}  -L {} -R {} -O {}  ".format(
							  normal_bam
							, normal_tag
							, self.args.exon_bed
							, self.args.ref
							, output1
							)
					)
#				cmd1 = cmd1 +" --disable-read-filter MateOnSameContigOrNoMappedMateReadFilter "
				cmd1 = cmd1 +  (" > {} 2>&1 ".format( log1 ) )

				print( cmd1)
				t_list2.add_thread( cmd1 )
#		t_list2.start_thread()


		"""
		step 02: collate all the normal VCFs \
			into a single callset with CreateSomaticPanelOfNormals. 
		"""
#		input2 = output1
		output2 = self.args.o_dir + "/" + "normal_panel.vcf.gz"
		log2 = output2 + ".log"

		cmd2 = ("gatk GenomicsDBImport -R {} -L {}  --genomicsdb-workspace-path pon_db "
					.format( self.args.ref ,
							 self.args.exon_bed

				))
		cmd2 = cmd2 + "-V "+ (" -V ".join( vcf_list ) ) 
		cmd2 = cmd2 +  (" > {} 2>&1 ".format( log2 ) )
		"""
		step 03:  #stimate cross-sample contamination using GetPileupSummaries and CalculateContamination
		"""
		output3 = self.args.o_dir + "/" + "normal_panel.vcf.gz"
		log3 = output3 + ".log"
		cmd3 = ("gatk CreateSomaticPanelOfNormals -R {}  --germline-resource {} -V gendb://pon_db -O {}".format(  
				self.args.ref,
				"genome/af-only-gnomad.raw.sites.hg19.vcf.gz",
				output3
				) )
		cmd3 = cmd3 +  (" > {} 2>&1 ".format( log3 ) )

 
		print(cmd2)

		os.system("rm pon_db/ -r ; " +  cmd2)

		print(cmd3)
		os.system(cmd3)


		"""
		step 04: Filter for confident somatic calls using FilterMutectCalls
		"""
		'''
gatk FilterMutectCalls \
-V somatic_m2.vcf.gz \
--contamination-table tumor_calculatecontamination.table \
-O 9_somatic_oncefiltered.vcf.gz
		'''





		pass





	def mutect2(self):

		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )
		os.system("mkdir -p "+self.args.o_dir)
#		t_list=test_thread.myThread_list(shell_cmd,c=35)
		gatk_path ="~/anaconda3/envs/exome-seq/bin/GenomeAnalysisTK"


		def shell_cmd(cmd):
			os.system(cmd)

		sub_thread = self.args.sub_t #4
		print("sub thread num:{}".format(sub_thread))
		t_list2=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list3=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list4=test_thread.myThread_list(shell_cmd,c=sub_thread)

		""" check build reference dict"""
		if os.path.isfile( ".".join( self.args.ref.split(".")[:-1] ) + ".dict" ) ==False:
			cmd = "picard CreateSequenceDictionary  R={}  O={}".format(self.args.ref, ".".join( self.args.ref.split(".")[:-1] ) + ".dict")

		""" check bai exit"""
		if os.path.isfile( file_list[0].replace(".bam",".bai") ) ==False:
			all_file = [ ele.repalce("T", "N") for ele in file_list] + file_list 
			for file in all_file:
				cmd = ( "samtools index {} {} ".format(file, file.replace(".bam",".bai")) )
				os.system( cmd )



		cmd_list = []
		for file in sorted(file_list ):
			eprint("processing file: ",file)

			tumor_bam = file
			tumor_tag =  fetch_tag(tumor_bam) 
			""" check normal bam"""
			if self.args.unify_n != "":
				normal_bam = self.args.unify_n
			else:
				file_item = file.split("/")
				normal_bam =  "/".join( file_item[:-1] ) + "/" + file_item[-1].replace("T","N")
				
#			print("normal_bam: ",normal_bam)
			""" check normal bam tag"""
			if self.args.n_tag != "":
				normal_tag =  fetch_tag( normal_bam) 
			else:
				normal_tag =  fetch_tag( normal_bam) 
			
			""" main cmd """
			if True:
				output1 = self.args.o_dir + "/" + file.split("/")[-1].replace(".bam", ".vcf")
				log1	= self.args.o_dir + "/" + file.split("/")[-1].replace(".bam", ".vcf.log")
						
				cmd1= ("gatk Mutect2 -I {} -normal {} -I {} -tumor {} -L {} -R {} -O {}  ".format(
							  normal_bam
							, normal_tag
							, tumor_bam
							, tumor_tag
							, self.args.exon_bed
							, self.args.ref
							, output1
							)
					)
				if self.args.n_p != "" :
					cmd1= cmd1+  ("  -pon {}  ".format( self.args.n_p ))

				""" germline resource"""
				cmd1 = cmd1 +  (" --germline-resource genome/af-only-gnomad.raw.sites.hg19.vcf.g"
								+ " --af-of-alleles-not-in-resource 0.0000025 "
								+ " --disable-read-filter MateOnSameContigOrNoMappedMateReadFilter  ")
				cmd1 = cmd1 +  (" > {} 2>&1 ".format( log1 ) )
				print(cmd1)
				print(normal_bam,"!!")

				"--af-of-alleles-not-in-resource 0.00003125   --germline-resource   /home/shepherd71c/workplace/neoantigen/CRC_patient_all/genome/af-only-gnomad.raw.sites.hg19.vcf.gz "
				t_list2.add_thread( cmd1 )
		t_list2.start_thread()
		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


