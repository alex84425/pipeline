import pyGeno.bootstrap as B

#Imports only the Y chromosome from the human reference genome GRCh37.75
#Very fast, requires even less memory. No download required.
#B.importGenome("Human.GRCh37.75_Y-Only.tar.gz")

#A dummy datawrap for humans SNPs and Indels in pyGeno's AgnosticSNP  format.
# This one has one SNP at the begining of the gene SRY
#B.importSNPs("Human.dummySRY_casava.tar.gz")
B.printDatawraps()

x1 = 119331103
g = Genome(name = "GRCh37.75")
for gene in g.get(Gene, {"start >=": x1, "end <" : x1, "chromosome.number" : "8"}) :
	print (gene)
