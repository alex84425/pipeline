#gp_MutSigCV merge.maf exome_full192.coverage.txt gene.covariates.txt test_output

#/data/workplace_alex/TLCGA_Algin_GATK_hg19_AnnoWithGRch37_merge/maf_strelka_output_bed_filter_src/merge_001-030_filter.maf
import sys, os
from pathlib import Path
import pandas as pd
def md_path(path):
        """ map_docker_path """
        "/home/shepherd71c/workplace/neoantigen/CRC_patient/merge_variant"
        home = str(Path.home())
        p = str( Path(path).resolve() )
        convert_path = p.replace(home, "/data")
        print( convert_path )
        return convert_path
        pass

#ori_maf = "merge_001-030_filter.maf"
ori_maf = sys.argv[1]
maf = md_path(ori_maf)

output_dir = "./"+("/".join( ori_maf.split("/")[:-1] ) + "/" + ".".join( ori_maf.split("/")[-1].split(".")[:-1] ))
print( "output_dir:", output_dir )
cmd = 'docker exec 99955a47abb6 bash -c "cd /data/git_file/musigCV && gp_MutSigCV {}  exome_full192.coverage.txt gene.covariates.txt {}"'.format(maf, maf  )
mv_cmd = "mv {}.* {}".format( ori_maf, output_dir)


""" commit command !"""
os.system( "mkdir -p {}".format( output_dir ) )
os.system( cmd )
os.system( mv_cmd )

sig_file = "./"+("/".join( ori_maf.split("/")[:-1] ) + "/" + ".".join( ori_maf.split("/")[-1].split(".")[:-1] )) + "/" + ori_maf.split("/")[-1] + ".sig_genes.txt"
df = pd.read_csv( sig_file, sep="\t" ) 

print( df[df["q"]<0.1] )
print( "num of driver:",len(df[df["q"]<0.1] ))


#maf_strelka_output_PASS/merge_strelka/merge_strelka.maf.sig_genes.txt


