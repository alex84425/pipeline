input=$1
printf $1,
#bedtools genomecov -ibam $input -bga -g genome.txt | awk '{if($4>0) total += ($3-$2)}END{print total}'
samtools depth $input    |  awk '{sum+=$3} END { print "Average = ",sum/NR}'
