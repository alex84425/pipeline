from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
import vcf
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		""" specific optopm"""
		parser.add_argument(	'--f' ,type=str, 
					default="",
					help='input directory')
		parser.add_argument(	'--log' ,type=str, 
					default="",
					help='input directory')

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		parser.add_argument(	'--f_f' ,type=str,
					default = "",
					help="ex: first 30 file '3:-10' ")




		self.args = parser.parse_args(args)

	def main(self,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		if self.args.f_f != "":
			start = int(self.args.f_f.split(":")[0])
			end = int(self.args.f_f.split(":")[1])
			file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )[start:end]

		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
		total_count = 0
		total_pass_count = 0


		for i,file in enumerate(file_list):
			count = 0
			pass_count = 0
			f_n = file.split("/")[-1]
			output = self.args.o_dir + "/" + f_n

			vcf_reader = vcf.Reader(filename= file )
			vcf_writer = vcf.Writer(open( output, 'w'), vcf_reader)
			header =  self.read_vcf_header( file ) 
			g_d ={ "N":header.split("\t")[-2],
				   "T":header.split("\t")[-1]
			}
			
			""" #CHROM  POS ID  REF ALT QUAL	FILTER  INFO	FORMAT  001N_merge_001  001T_merge_001 """
			for record in vcf_reader:
				'''
				print( record.genotype(g_d["N"]))		
				print( record.genotype(g_d["N"])["AF"] )		
				print( record.genotype(g_d["T"])["AF"] )		
				input()
				'''

				if self.filter_record(record, g_d):
					vcf_writer.write_record(record)
					pass_count += 1

				count += 1 
			print( "{}: {} -> {}, ({}%)".format(f_n, count, pass_count, round(100*pass_count/count,2 ) ) )
			total_count +=  count
			total_pass_count +=  pass_count

		if self.args.log !="":
			log = "{}/{}".format(self.args.o_dir,self.args.log)
		else:
			log = "{}/log_filter.txt".format(self.args.o_dir)
		with open(log,"w") as f:
			print("log:", log)
			filter_para = ( "filter log :" +  self.args.f )

			result1 = ( "total: {} -> {}, ({}%)".format(total_count, total_pass_count, round(100*total_pass_count/total_count,2 ) ) )
			result2 = ( "avg: {} -> {}, ({}%)".format(
										  round( total_count/len(file_list),2 )
										, round( total_pass_count/len(file_list),2 )
										, round(100*total_pass_count/total_count,2 ) ) )
			f.write( filter_para + "\n" )
			f.write( result1 + "\n" )
			f.write( result2 + "\n" )
			print( filter_para  )
			print( result1  )
			print( result2  )



	def filter_record(self, record, g_d):
		condition_list = self.args.f.split(",")
		for c in condition_list:
			if ">" in c or "<" in c:
				g = c.split("/")[0]
				if ">" in c:
					loc = ( c.split("/")[1].find(">"))
				elif "<" in c :

					loc = ( c.split("/")[1].find("<"))

				target = c.split("/")[1][:loc]
				cut_off = float( c.split("/")[1][loc+1:] )

				'''
				print(c[loc])
				print( target)
				print( cut_off)
#				print( record.genotype( g_d[g] ) )
				'''
#				input()


				v = record.genotype( g_d[g] )[target] 
				""" hetero mutation  """
				if type( v ) ==type( [] ):
					if target == "AD":
						v = min( v )
					elif target == "AF":
						
						v = max( v )
						return False
						continue

				if ">" in c :
					b =  v  > cut_off
				elif "<" in c :
					b =  v  < cut_off
				""" """
				if b==False:
					return False

		return True
		pass
	

#			cmd = "fastq_quality_filter -q 30 -i {} -o {}  -p 80 -Q33".format( file, output  )
#			print( cmd)
	def read_vcf_header(self,vcf):
		""" https://stackoverflow.com/questions/32224363/python-convert-comma-separated-list-to-pandas-dataframe i """
		""" CHROM  POS ID  REF ALT QUAL FILTER  INFO	FORMAT  NORMAL  TUMOR """
		vcf_list = []
		with open(vcf, "r") as f:
			lines = f.readlines()
			for i,line in enumerate(lines):
				if line[0]!="#":
					header_loc = i-1
					break
			vcf_content = lines[header_loc+1:]
#		   vcf_content = vcf_content[:10]
			vcf_header = lines[ header_loc].strip()
		return vcf_header


if __name__ == '__main__':

	ob=ob_name()
	#assign variable


