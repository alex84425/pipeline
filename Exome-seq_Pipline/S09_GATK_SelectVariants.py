import argparse
import sys
import os
import re
from functools import wraps
import datetime
import time
import operator
# Fixing random state for reproducibility
import argparse
import sys
import gzip
#from Bio import SeqIO
import glob
import numpy as np
import pandas as pd
import test_thread


from fnmatch import fnmatch

def all_file_subdir(target, pattern):
		root = target
#		pattern = "*.fastq"
		file_list =[]
		for path, subdirs, files in os.walk(root):
			for name in files:
				if fnmatch(name, pattern):
					file_list.append( os.path.join(path, name) )
		return file_list



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


def TimeConsume(f):
	@wraps(f)
	def wrapper(*args,**kwargs):
		start = datetime.datetime.now()
		f(*args,**kwargs)
		end = datetime.datetime.now()
		tc = end - start
		print(  "time consume: {:02d}:{:02d}".format( tc.days, tc.seconds  ) )
	return wrapper

@TimeConsume
class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
#		file_list=  glob.glob("{}/*".format(self.args.i_dir ))
		self.main()
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python  ~/pipeline/Exome-seq_Pipline/S09_GATK_SelectVariants.py  --i_dir GATK_Mutect2/ --key "*.vcf"  --o_dir GATK_Mutect2_selected')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input dir')
		parser.add_argument(	'--o_dir' ,type=str, 
					help='output dir')
		parser.add_argument(	'--key' ,type=str, 
					default= "*",
					help='file keyword')
		parser.add_argument(	'--t' ,type=int, 
					default= 10,
					help='thread for tool')
		parser.add_argument(	'--sub_t' ,type=int, 
					default= 10,
					help='thread for force parallel')

		self.args = parser.parse_args(args)
		self.args = parser.parse_args(args)

	def main(self):
		""" 
		refernce source : https://www.biostars.org/p/19230/
		flag explain: https://www.samformat.info/sam-format-flag

		4: ummap
		64: R1
		128: R2
		"""
		file_list = sorted( glob.glob("{}/{}".format(self.args.i_dir, self.args.key) ) )
		os.system( "mkdir -p {}".format(self.args.o_dir)  )

		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=self.args.sub_t)

		for file in file_list:
			""" split variant intosnp and indel  """
			f_n = file.split("/")[-1]
			output_snp = self.args.o_dir + "/" +  f_n.replace(".vcf",".snp.vcf")
			output_indel = self.args.o_dir + "/" +  f_n.replace(".vcf",".indel.vcf")
			cmd_snp = "gatk  SelectVariants  -select-type SNP  -V {}  -O {} ".format(file, output_snp)
			cmd_indel = "gatk  SelectVariants  -select-type INDEL  -V {}  -O {} ".format(file, output_indel)
#			print( cmd_snp )
#			print( cmd_indel )

			""" filter variant """
			output2_snp = self.args.o_dir + "/" +  f_n.replace(".vcf",".snp.filter.vcf")
			output2_indel = self.args.o_dir + "/" +  f_n.replace(".vcf",".indel.filter.vcf")
			filter_snp_para = "\"QD < 2.0 || MQ < 40.0 || FS > 60.0 || SOR > 3.0 ||  MQRankSum < -12.5 || ReadPosRankSum < -8.0\""
			filter_indel_para = "\" QD < 2.0 || FS > 200.0 || SOR > 10.0 ||  MQRankSum < -12.5 || ReadPosRankSum < -8.0  \""
			""" para source """
			filter_snp_para = "\"  QD < 2.0 || QUAL < 30.0 || SOR > 3.0 || FS > 60.0 || MQ < 40.0 ||  MQRankSum < -12.5 || ReadPosRankSum < -8.0    \""
			filter_indel_para = "\" QD < 2.0 || QUAL < 30.0 || FS > 200.0 || ReadPosRankSum < -20.0  \""

			cmd_snp_f = "gatk  VariantFiltration  -V {}  --filter-expression  {}  --filter-name \"PASS\"  -O {}".format(output_snp, filter_snp_para, output2_snp)
			cmd_indel_f = "gatk  VariantFiltration  -V {}  --filter-expression  {}  --filter-name \"PASS\"  -O {}".format(output_indel, filter_indel_para, output2_indel)

#			print( cmd_snp_f )
#			print( cmd_indel_f )

			""" merge snp and indel """
			output3 = self.args.o_dir + "/" +  f_n.replace(".vcf",".pass.vcf")
			cmd_merge = "gatk MergeVcfs  -I {} -I {}  -O {}".format(output2_snp, output2_indel, output3 )

			whole_cmd = ";".join( [cmd_snp, cmd_indel, cmd_snp_f, cmd_indel_f, cmd_merge] )
#			whole_cmd = ";".join( [cmd_snp, cmd_indel, cmd_snp_f, cmd_indel_f] )
#			whole_cmd = ";".join( [cmd_indel, cmd_indel_f] )
#			print( whole_cmd)
			
			t_list.add_thread( whole_cmd )
			t_list.add_thread( cmd_merge )
		t_list.start_thread()



if __name__ == '__main__':

	ob=ob_name()
	#assign variable



