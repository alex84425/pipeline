from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob

import pandas as pd
import numpy as np
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o' ,type=str, 
					default="",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')

		parser.add_argument(	'--rename_barcode' ,type=str, 
					default="",
					help="example: CRC{}_PDX")

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')

		

		self.args = parser.parse_args(args)

	def main(self):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		
#		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2

		df_list = []
		os.system( "mkdir -p {} ".format( self.args.o_dir ) )
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			output = self.args.o_dir + "/" + f_n
#			cmd = "fastq_quality_filter -q 30 -i {} -o {}  -p 80 -Q33".format( file, output  )
			df = self.read_maf( file )
			df = self.filter_maf( df )
#			if self.args.rename_barcode != "":
#				df = self.rename_barcode( df )
			print("{}\t{}".format(f_n, len(df) ) )
			df.to_csv(output, sep="\t", index=False)

	
#			t_list.add_thread( cmd )
#		t_list.start_thread()

	def filter_maf(self, df):
		v_col =[ 'Frame_Shift_Del',
			  'Missense_Mutation',
			  'Nonsense_Mutation',
			  'Multi_Hit',
			  'Frame_Shift_Ins',
			  'In_Frame_Ins',
			  'Splice_Site',
			  'In_Frame_Del']
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df

	def rename_barcode(self, df):

		#df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].apply( lambda x: "CRC{}_PDX".format(x) )
		df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].apply( lambda x: self.args.rename_barcode.format(x) )
		print(  df["Tumor_Sample_Barcode"] )
		return df


	def read_maf(self, maf):
		maf_df = pd.read_csv(maf, sep = "\t", comment="#")
#		print( maf_df )
		return maf_df
#		input()

		pass
""" 
Hugo_Symbol Entrez_Gene_Id  Center  NCBI_Build  Chromosome  Start_Position  End_Position    Strand  Variant_Classification  Variant_Type    Reference_Allele    Tumor_Seq_Allele1   Tumor_Seq_Allele2   dbSNP_RS    dbSNP_Val_Status    Tumor_Sample_Barcode    Matched_Norm_Sample_Barcode Match_Norm_Seq_Allele1  Match_Norm_Seq_Allele2  Tumor_Validation_Allele1    Tumor_Validation_Allele2    Match_Norm_Validation_Allele1   Match_Norm_Validation_Allele2   Verification_Status Validation_Status   Mutation_Status Sequencing_Phase    Sequence_Source Validation_Method   Score   BAM_File    Sequencer   Tumor_Sample_UUID   Matched_Norm_Sample_UUID    HGVSc   HGVSp   HGVSp_Short Transcript_ID   Exon_Number t_depth t_ref_count t_alt_count n_depth n_ref_count n_alt_count all_effects Allele  Gene    Feature Feature_type    Consequence cDNA_position   CDS_position    Protein_position    Amino_acids Codons  Existing_variation  ALLELE_NUM  DISTANCE    STRAND_VEP  SYMBOL  SYMBOL_SOURCE   HGNC_ID BIOTYPE CANONICAL   CCDS    ENSP    SWISSPROT   TREMBL  UNIPARC RefSeq  SIFT    PolyPhen    EXON    INTRON  DOMAINS AF  AFR_AF  AMR_AF  ASN_AF  EAS_AF  EUR_AF  SAS_AF  AA_AF   EA_AF   CLIN_SIG    SOMATIC PUBMED  MOTIF_NAME  MOTIF_POS   HIGH_INF_POS    MOTIF_SCORE_CHANGE  IMPACT  PICK    VARIANT_CLASS   TSL HGVS_OFFSET PHENO   MINIMISED   ExAC_AF ExAC_AF_AFR ExAC_AF_AMR ExAC_AF_EAS ExAC_AF_FIN ExAC_AF_NFE ExAC_AF_OTH ExAC_AF_SAS GENE_PHENO  FILTER  flanking_bps    vcf_id  vcf_qual    ExAC_AF_Adj ExAC_AC_AN_Adj  ExAC_AC_AN  ExAC_AC_AN_AFR  ExAC_AC_AN_AMR  ExAC_AC_AN_EAS  ExAC_AC_AN_FIN  ExAC_AC_AN_NFE  ExAC_AC_AN_OTH  ExAC_AC_AN_SAS  ExAC_FILTER gnomAD_AF   gnomAD_AFR_AF   gnomAD_AMR_AF   gnomAD_ASJ_AF   gnomAD_EAS_AF   gnomAD_FIN_AF   gnomAD_NFE_AF   gnomAD_OTH_AF   gnomAD_SAS_AF   vcf_pos

"""

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


