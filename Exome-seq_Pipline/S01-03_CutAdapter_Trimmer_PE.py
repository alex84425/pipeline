from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
import list_lib as ll
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.qc_and_trimmer_all(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='ex: python ../S01-03_CutAdapter_Trimmer_PE.py --i_dir Rawread/ --o_dir QC_dir --t 20')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--t' ,type=int, 
					default = 20,
					help='input directory')
		parser.add_argument(	'--adaptor_path' ,type=str, 
					default = "adapters/TruSeq3-PE.fa",
					help='input the path of adapter which contain TruSeq3-SE.fa or TruSeq3-PE.fa')
		parser.add_argument(	'--PEorSE' ,type=str, 
					help='PE or SE')

		""" file selector """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='keywoed')
		parser.add_argument(	'--include' ,type=str, 
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str, 
					default = "",
					help='key exlude')

		self.args = parser.parse_args(args)

	def qc_and_trimmer_all(self,path, output_dir):
		""" using Trimmomatic"""
		os.system("cp ~/adapters/ ./ -r")
		
		#file_list = os.popen("ls "+path+ " |grep '.fastq' ").read().strip().split("\n")
		file_list = sorted(  glob.glob( "{}/{}".format( self.args.i_dir,"/*" )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		file_list_R1 =  ll.list_iexclude( file_list, "R1", "in" )
		file_list_R2 =  ll.list_iexclude( file_list, "R2", "in" )

#		file_list_R1 = glob.glob(self.args.i_dir+"/*R1*")
#		file_list_R2 = glob.glob(self.args.i_dir+"/*R2*")

		print("file_list_R1:",file_list_R1)
		print("file_list_R2",file_list_R2)
		os.system("mkdir -p "+self.args.o_dir)

		
		SE_or_PE = self.args.PEorSE
		if len(file_list_R2) == len(file_list_R1):
			print("process as PE!")
			SE_or_PE = "PE"
			os.system("mkdir -p  QC_Rawread_unpair")
		elif len(file_list_R2) == 0:
			print("process as SE!")
		else:
			print("PE data lost!")
			print(len(file_list_R2), len(file_list_R1 ) )
			return 0	


		threads_num = self.args.t 
		for R1_file in file_list_R1:
			print(R1_file)
			R2_file = R1_file.replace("R1","R2")

			if SE_or_PE == "SE":
				R1_file_output= self.args.o_dir +"/"+ R1_file.split("/")[-1]
				cmd="trimmomatic SE {}  {}  ILLUMINACLIP:{}/TruSeq3-SE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 -threads {}".format( R1_file, R1_file_output, self.args.adaptor_path, threads_num  )
				print(cmd)
				os.system(cmd)
			elif SE_or_PE == "PE":
				#001T_R1_001_PE.fastq
				#001T_R1_001.fastq.gz
				R1_pair = self.args.o_dir +"/"+ R1_file.split("/")[-1].replace(".gz","")
				R2_pair = self.args.o_dir +"/"+ R2_file.split("/")[-1].replace(".gz","")

				R1_unpair = "QC_Rawread_unpair/"+ R1_file.split("/")[-1].replace(".gz","")
				R2_unpair = "QC_Rawread_unpair/"+ R2_file.split("/")[-1].replace(".gz","")
				
				cmd = "trimmomatic PE {} {} {} {} {} {} ILLUMINACLIP:{}:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 -threads {}".format( R1_file, R2_file, R1_pair, R1_unpair, R2_pair ,R2_unpair, self.args.adaptor_path, threads_num  )
				print(cmd)
				os.system(cmd)
				pass	
		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


