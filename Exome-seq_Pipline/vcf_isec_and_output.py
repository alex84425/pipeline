from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
import pandas as pd
import vcf
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )
from itertools import dropwhile
if sys.version_info[0] < 3: 
	from StringIO import StringIO
else:
	from io import StringIO

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ~/pipeline/Exome-seq_Pipline/vcf_isec.py  --i_dir test_vcf/ --key "*vcf*" --include "bf"')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')
		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		self.args = parser.parse_args(args)

	def main(self,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		print(file_list)
		if self.args.o_dir != None :
			os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2

		df_list = []
		for i,file in enumerate(file_list):
			df = self.read_vcf(file)
			df_list.append( df )


		
		output_header = [ "_".join( ele.split("/")[-1].split(".")[0:2] )   for ele in file_list]
		print( output_header )
		overlap_2d = []
		overlap_2d_df = []
		for i,df_a in enumerate(df_list):
			overlap_1d = []
			overlap_1d_df = []
			for j,df_b in enumerate(df_list):

#				pd.merge()
#				print(df_a.columns)
				merge_col = ['CHROM', 'POS', 'REF', 'ALT']
				merge_df = pd.merge(df_a, df_b,  how='inner'
					, left_on=merge_col
					, right_on=merge_col 
				)

				overlap_1d.append( "{}({}%)".format( len( merge_df ), round(len( merge_df )*100/len(df_a),2) ) )
				overlap_1d_df.append( merge_df )
#				overlap_1d.append( "{}".format( len( merge_df ), round(len( merge_df )*100/len(df_a),2) ) )

			
			overlap_2d.append( overlap_1d )
			overlap_2d_df.append( overlap_1d_df )




#		print( overlap_2d )
#				print( merge_df)

		output_df = pd.DataFrame( overlap_2d
			,columns= output_header 
		)
		output_df.index= output_header


		def skip(x):
			return x.split("(")[0]
			pass

		print( output_df )
		output_df = output_df.applymap( skip )
			
		""" output isec vcf"""
#		print( overlap_2d_df[0][0].columns )

		lr = 1
		for i in range( len(file_list) ):
			if i%2 ==0:
				if 	lr ==0:
					row = i + 1 
					col = i 
				else:
					row = i 
					col = i + 1
#				print( row, col )
				if self.args.o_dir != None :
					f_n = file_list[row].split("/")[-1]
					
#					print("!!!!!!!!",f_n)
					output = "{}/{}".format( self.args.o_dir, f_n)
					self.output_vcf_isec(output, overlap_2d_df, file_list)
#					input()

		if self.args.o_dir!=None:
			output_df.to_csv(self.args.o_dir + "/" + "isec_table.tsv", sep="\t", index=False)
#			f_n = file.split("/")[-1]
#			output = self.args.o_dir + "/" + f_n
#			cmd = "fastq_quality_filter -q 30 -i {} -o {}  -p 80 -Q33".format( file, output  )
#			print( cmd)
#			t_list.add_thread( cmd )
#		t_list.start_thread()

	#def output_vcf_isec(self, vcf):
	def output_vcf_isec(self, output_merge, overlap_2d_df, file_list):
#		output_merge = "merge_tmp.vcf"
		output = output_merge + ".content"
		output_h = output_merge + ".header"
		tmp_df =  overlap_2d_df[0][0]["record_x"] 
		tmp_df.to_csv( output, sep = "\t", index = False   )
		os.system("sed -i 's/^\"//g' {}".format(output) )
		os.system("sed -i 's/\"$//g' {}".format(output) )
		with open( output_h, "w") as f:
			f.write( self.read_vcf_header( file_list[0] ) )
		os.system( "cat {} {} > {}".format(output_h, output, output_merge ) )
#		os.system( "rm {} {} ".format(output_h, output) )

	def read_vcf(self,vcf):
		""" https://stackoverflow.com/questions/32224363/python-convert-comma-separated-list-to-pandas-dataframe i """
		""" CHROM  POS ID  REF ALT QUAL	FILTER  INFO	FORMAT  NORMAL  TUMOR """
		vcf_list = []
		with open(vcf, "r") as f:
			lines = f.readlines()
			for i,line in enumerate(lines):
				if line[0]!="#":
					header_loc = i-1 
					break
			vcf_content = lines[header_loc+1:]
#			vcf_content = vcf_content[:10]
			vcf_header = lines[ header_loc].strip() + "\trecord"
			df = pd.DataFrame([ sub.strip().split("\t") +[sub.strip()]   for sub in vcf_content]  , columns= vcf_header[1:].split("\t") )
		return df

	def read_vcf_header(self,vcf):
		""" https://stackoverflow.com/questions/32224363/python-convert-comma-separated-list-to-pandas-dataframe i """
		""" CHROM  POS ID  REF ALT QUAL	FILTER  INFO	FORMAT  NORMAL  TUMOR """
		vcf_list = []
		with open(vcf, "r") as f:
			lines = f.readlines()
			for i,line in enumerate(lines):
				if line[0]!="#":
					header_loc = i-1 
					break
			vcf_content = lines[header_loc+1:]
#			vcf_content = vcf_content[:10]
			vcf_header = "".join(  lines[ :header_loc+1] )
		return vcf_header





if __name__ == '__main__':

	ob=ob_name()
	#assign variable


