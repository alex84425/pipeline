from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO


def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.bedtool_cmd( self.args.i_dir, self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--bed' ,type=str, 
					help='input .bed file')
		parser.add_argument(	'--t' ,type=int, 
					default=10		,
					help='input thread number')
		parser.add_argument(	'--key' ,type=str, 
					default="*bam"		,
					help='file keyword')

		self.args = parser.parse_args(args)

	def bedtool_cmd(self,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' |grep -v bai |grep N| grep printread").read().strip().split("\n")
		file_list = glob.glob( "{}/{}".format(self.args.i_dir, self.args.key) )
		print("file_list",file_list)
		os.system("mkdir -p "+self.args.o_dir)

		# thread method
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 1
		for file in file_list:

			input1 = file
			output1 = self.args.o_dir + "/" +file.split("/")[-1]
			cmd = "bedtools intersect -abam {}  -b {} > {}".format(input1, self.args.bed, output1)
			print( cmd )
#			os.system( cmd )
			t_list.add_thread( cmd)
		t_list.start_thread()




		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


