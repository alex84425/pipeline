from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.gunzip_all(self.args.dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--dir' ,type=str, 
					help='input .fasta or .fastq')
		self.args = parser.parse_args(args)

	def gunzip_all(self,path):
		file_list = os.popen("ls "+path+ " |grep '.gz' ").read().strip().split("\n")
		def shell_cmd(cmd):
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd)
		t_list.sleep_time = 2

		for file in file_list:
			cmd = "gunzip "+file
			t_list.add_thread( cmd )
		t_list.start_thread()
#		print()
		

		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


