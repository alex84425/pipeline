from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.qc_and_trimmer_all(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')

		self.args = parser.parse_args(args)

	def qc_and_trimmer_all(self,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.fastq' |grep R1").read().strip().split("\n")
		os.system("mkdir -p "+self.args.o_dir)
		# thread method

		# paired-read		
		for file in file_list:
			eprint("processing file: ",file)
			basename=  file.replace("R1","merge").split(".")[0]
			cmd = "AdapterRemoval --file1 {} --file2 {} --basename {} --threads 40 ".format( file,file.replace("R1","R2"),  basename  )

			
			R1 = file.replace("R1","merge").split(".")[0] + ".pair1.truncated "
			R2= file.replace("R1","merge").split(".")[0] + ".pair2.truncated "
			#move file
			cmd2= "mv {} {} ".format(" *"+basename+"*  ",self.args.o_dir)
			os.system(cmd2)

			#rename 
			cmd3 = "cd "+self.args.o_dir+" ;"+"mv "+R1 +" "+ file.split(".")[0]+".fastq ;"
			cmd4 = "cd "+self.args.o_dir+" ;"+"mv "+R2 +" "+ file.split(".")[0].replace("R1","R2")+".fastq ;"
#			os.system(cmd)
#			os.system(cmd2)
			os.system(cmd3)
			os.system(cmd4)

		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


