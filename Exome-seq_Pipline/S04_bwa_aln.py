from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.bwa_aln(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--ref' ,type=str, 
					help='input directory')

		parser.add_argument(	'--t' ,type=str, 
					default = 16,
					help='thread num')

		""" file selector """
		parser.add_argument(	'--key' ,type=str,
					default = "*",
					help='keywoed')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')


		self.args = parser.parse_args(args)

	def bwa_aln(self,ref,path, output_dir):
		#file_list = os.popen("ls "+path+ " |grep '.fastq' |grep _R1_ ").read().strip().split("\n")

		file_list = sorted(glob.glob( "{}/{}".format(self.args.i_dir, self.args.key) ) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )

		file_list = [ ele.split("/")[-1] for ele in  file_list if "_R1_" in ele]

		os.system("mkdir -p "+self.args.o_dir)
		# thread method

		""" check build index"""
		cmd = "bwa index {}".format(self.args.ref)
		if os.path.isfile(self.args.ref+".bwt") == False:
			os.system(cmd )
			print(self.args.ref+".bwt")
			pass
		else:
			eprint("bwa index exist!")


		for file in file_list:
			eprint("processing file: ",file)
		
			""" check pair-end or single end"""
			if os.path.isfile( path+"/"+file.replace("_R1_","_R2_") ):
				log_file =  output_dir+"/"+file.replace("_R1_","_merge_").replace("fastq","sam.log")
				cmd= "bwa mem {} {}  {}  -t {} > {} 2> {}".format(ref ,path+"/"+file,path+"/"+file.replace("_R1_","_R2_"),self.args.t ,output_dir+"/"+file.replace("_R1_","_merge_").replace("fastq","sam").replace(".gz",""),log_file   )
				print("run pair-end!")
				print(cmd)	
				os.system(cmd)
			else:
				log_file =  output_dir+"/"+file.replace("_R1_","_SE_").replace("fastq","sam.log").replace(".gz","")
				cmd= "bwa mem {} {}	-t {} > {} 2> {}".format(ref ,path+"/"+file,self.args.t , output_dir+"/"+file.replace("_R1_","_SE_").replace("fastq","sam").replace(".gz",""), log_file   )
				print("run single-end!")
				print(cmd)	
				os.system(cmd)

		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


