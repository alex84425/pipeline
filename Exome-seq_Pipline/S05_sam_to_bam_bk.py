from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.sam_to_bam(self.args.ref,self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--ref' ,type=str, 
					help='input directory')
		parser.add_argument(	'--p' ,type=str, 
					help='input picard.jar'
					,default="")

		self.args = parser.parse_args(args)

	def sam_to_bam(self,ref,path, output_dir):
		file_list = os.popen("ls "+path+ " |grep '.sam' ").read().strip().split("\n")
		os.system("mkdir -p "+self.args.o_dir)

		#check reference
		from pathlib import Path
		fai_path = Path(self.args.ref+".fai")
		if (fai_path.is_file())==False:
			os.system("	samtools faidx {}".format(self.args.ref))


		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c=5)
		for file in file_list:
			eprint("processing file: ",file)
			#cmd = "java -jar {} SortSam   INPUT={}   OUTPUT={}   SORT_ORDER=coordinate".format( self.args.p,path+"/"+file ,self.args.o_dir+"/"+file.replace(".sam",".bam"))
			cmd = "picard SortSam   INPUT={}   OUTPUT={}   SORT_ORDER=coordinate".format( path+"/"+file ,self.args.o_dir+"/"+file.replace(".sam",".bam"))

#			print(cmd)	
#			os.system(cmd)
			t_list.add_thread( cmd )
		t_list.start_thread()

		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


