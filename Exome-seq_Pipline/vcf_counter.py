from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 

					help='input directory')
		parser.add_argument(	'--key' ,type=str, 
					default = "*vcf*",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')

		parser.add_argument(	'--f' ,type=str, 
					default = 'QSS>100 && QSS_NT>100 && SomaticEVS > 10 && MQ>55 && INFO/DP>90',
					help='select/exclude sites for which the expression is true ')

		self.args = parser.parse_args(args)

	def main(self,path, output_dir):
		""" bcftools view -i 'QSS>100 && QSS_NT>100 && SomaticEVS > 10 && MQ>55 && INFO/DP>90' -f PASS Strelka_T1R1_vs_Ctrl_somatic_snvs.vcf.gz """
		""" gzip -c  strelka_file/001.somatic.snvs.vcf  | bcftools view  -i 'QSS>100 && QSS_NT>100 && SomaticEVS > 10 && MQ>55 && INFO/DP>90' -f PASS  |  grep -v ^# """
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )))
		
		os.system("mkdir -p "+self.args.o_dir)
		# thread method
		def shell_cmd(cmd):
			print(cmd)
			os.system(cmd)
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.sub_t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 2
		for i,file in enumerate(file_list):
			f_n = file.split("/")[-1]
			output = self.args.o_dir + "/" + f_n
			#cmd = "fastq_quality_filter -q 30 -i {} -o {}  -p 80 -Q33".format( file, output  )
			filter_para = self.args.f

			with open(file, 'r') as f :
				line = [ ele for ele in f.readlines() if ele[0] != '#']
				print( "{}: {}".format(file, len(line))) 

#			print( cmd)

#			t_list.add_thread( cmd )
			
		
#		t_list.start_thread()

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


