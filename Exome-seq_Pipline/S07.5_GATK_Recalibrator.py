from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll



def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.co_clean(self.args.i_dir,self.args.o_dir)
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='Script to learn basic argparse')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					default="co_clean",
					help='input directory')

		""" know site new """
		parser.add_argument(	'--KG_hg19_indel' ,type=str,
					#default="~/ref_seq/dbsnp/GRch37/1000G_phase1.indels.hg19.sites.vcf",
					default="genome/1000G_phase1.indels.hg19.sites.vcf",
					help='input 1000G_phase1.indels.hg19.vcf')

		parser.add_argument(	'--M_KG_indel' ,type=str,
					#default="~/ref_seq/dbsnp/GRch37/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf",
					default="genome/Mills_and_1000G_gold_standard.indels.hg19.sites.vcf",
					help='input Mills_and_1000G_gold_standard.indels.hg19.vcf')
		parser.add_argument(	'--dbsnp' ,type=str,
					#default="~/ref_seq/dbsnp/GRch37/dbsnp_138.hg19.vcf",
					default="genome/dbsnp_138.hg19.vcf",
					help='input dbsnp_138.hg19.vcf')

		parser.add_argument(	'--KG_omni' ,type=str,
					#default="~/ref_seq/dbsnp/GRch37/dbsnp_138.hg19.vcf",
					default="genome/1000G_omni2.5.hg19.sites.vcf",
					help='input 1000G_omni2.5')


		parser.add_argument(	'--exon_bed' ,type=str, 
#					default="region_bed/S07604514_AllTracks.bed",
					default=None,
					help='input bed file')

		parser.add_argument(	'--ref' ,type=str, 
#					default= "genome/95_37.fa",
					help='input_ref')
		parser.add_argument(	'--t' ,type=str, 
					default= 30,
					help='number of threads')
		parser.add_argument(	'--sub_t' ,type=int, 
					default= 10,
					help='number of threads')
		""" filter seletor """
		parser.add_argument(	'--key' ,type=str,
					default = "*bam",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')



		self.args = parser.parse_args(args)

	def co_clean(self,path, output_dir):
#		file_list = os.popen("ls "+path+ " |grep '.bam' ").read().strip().split("\n")
#		file_list = sorted( glob.glob( "{}/*bam".format(self.args.i_dir) ) )
		file_list = sorted( glob.glob( "{}/{}".format(self.args.i_dir, self.args.key) ) )
#		file_list = file_list[0:1]
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )




		print( file_list )
		print( "file_num:",len(file_list) )
		os.system("mkdir -p "+self.args.o_dir)

#		t_list=test_thread.myThread_list(shell_cmd,c=35)
		gatk_path ="~/anaconda3/envs/exome-seq/bin/GenomeAnalysisTK"

		""" check genome_dcit"""
#		print(  ".".join(self.args.ref.split(".")[:-1])+".dict"	)
		if os.path.isfile(  ".".join(self.args.ref.split(".")[:-1])+".dict"   ) == False:
			os.system( "picard CreateSequenceDictionary R={}  O={}".format(self.args.ref, ".".join(self.args.ref.split(".")[:-1])+".dict") )
#

		def shell_cmd(cmd):
			os.system(cmd)

		sub_thread = self.args.sub_t #4
		print("sub thread num:{}".format(sub_thread))
		t_list2=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list3=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list4=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list5=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list6=test_thread.myThread_list(shell_cmd,c=sub_thread)
		t_list7=test_thread.myThread_list(shell_cmd,c=sub_thread)


		cmd_list = []
		for file in file_list:
			"""
			bedtools intersect -abam T1R1_0.md.bam -b $exon_bed > T1R1_0.md.exon.bam

			gatk  BaseRecalibrator   --input T1R1_0.md.exon.bam   --output T1R1.recal.table   --tmp-dir /tmp   -R $ref	 --known-sites $dbsnp   --known-sites $Mills_and_1000G_gold_standard   --verbosity INFO


			gatk ApplyBQSR   -R $ref   --input T1R1_0.md.exon.bam   --output T1R1.recal.bam	--create-output-bam-index true   --bqsr-recal-file T1R1.recal.table
			"""

		
			input1 = file
			output1 = self.args.o_dir+ "/" + (file.split("/")[-1][0:4]+".bam").replace(".bam",".exon.bam")
			cmd1 = "bedtools intersect -abam {} -b {} > {}".format(file, self.args.exon_bed ,output1)
			eprint("processing file: ",file)


			

			if self.args.exon_bed != "":
				input2 = output1
				eprint("processing file: ",file)
			else:
				input2 = file


			output2 = output1.replace(".exon.bam",".recal.table")
			cmd2 = "gatk  BaseRecalibrator   --input {}   --output {}  --tmp-dir /tmp   -R {}	 --known-sites  {}  --known-sites {}   --known-sites  {}  --known-sites {}  --verbosity INFO".format(input2, output2, self.args.ref, self.args.dbsnp, self.args.KG_hg19_indel, self.args.M_KG_indel, self.args.KG_omni)
			cmd2 = cmd2 + " --known-sites {}  ".format( "genome/1000G_phase1.snps.high_confidence.hg19.sites.vcf")

			if self.args.exon_bed != "":
				input3 = output1
				eprint("processing file: ",file)
			else:
				input3 = file

			output3 = output1.replace(".exon.bam",".recal.bam")
			input3_2 = output2.replace(".exon.bam",".recal.table")
			cmd3 = "gatk ApplyBQSR   -R {}   --input {}  --output {}   --create-output-bam-index true   --bqsr-recal-file {}".format(self.args.ref, input3, output3, output2)



			""" below is germline calling"""

			input4= output3
			output4 = output1.replace(".exon.bam",".g.vcf")

			if self.args.exon_bed == None:
				cmd4="gatk HaplotypeCaller	   -R {}	   -I {}		   --dbsnp {}	   -O {}   --emit-ref-confidence GVCF".format(self.args.ref, input4, self.args.dbsnp, output4)
			else:
				cmd4="gatk HaplotypeCaller	   -R {}	   -I {}	   -L {}	--dbsnp {}	   -O {}   --emit-ref-confidence GVCF".format(self.args.ref, input4, self.args.exon_bed, self.args.dbsnp, output4)

			input5 = output4
			cmd5 = "gatk IndexFeatureFile -F {}".format(input5 )

			output6 = output1.replace(".exon.bam",".vcf")
			if self.args.exon_bed == None:
				cmd6 = "gatk GenotypeGVCFs   -R {}  --dbsnp {}  -V {}  -O {}".format(self.args.ref, self.args.dbsnp, output4, output6  )
			else:
				cmd6 = "gatk GenotypeGVCFs   -R {}   -L {}   --dbsnp {}  -V {}  -O {}".format(self.args.ref, self.args.exon_bed, self.args.dbsnp, output4, output6  )

			if self.args.exon_bed != "":
				print(cmd1)
			print(cmd2)
			print(cmd3)
#			print(cmd4)
#			print(cmd5)
#			print(cmd6)

			t_list2.add_thread( cmd1 )
			t_list3.add_thread( cmd2 )
			t_list4.add_thread( cmd3 )
			t_list5.add_thread( cmd4 )
			t_list6.add_thread( cmd5 )
			t_list7.add_thread( cmd6 )
#			cmd_list.append( "{};{};{};".format(cmd2,cmd3,cmd4) )

#		'''

		if self.args.exon_bed != "":
			t_list2.start_thread()
		
		t_list3.start_thread()
		t_list4.start_thread()
#		'''
		""" Haplottypecaller"""
#		t_list5.start_thread()
#		t_list6.start_thread()
#		t_list7.start_thread()




#			os.system(cmd)	
#			t_list.add_thread( cmd )
#		t_list.start_thread()
		pass

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


