#!/bin/sh

#ref="genome/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa"
ref="genome/ucsc.hg19.fasta"
python ~/pipeline/Exome-seq_Pipline/S01-03_CutAdapter_Trimmer_PE.py --i_dir Rawread/ --o_dir QC_Rawread --t 16
#python ~/pipeline/Exome-seq_Pipline/S01-03_CutAdapter_Trimmer_PE.py --i_dir Rawread/ --o_dir QC_Rawread --t 16 --exclude "/001,/002,/004,/006"

###3
cat QC_Rawread/E06T_R1_001.fastq  >> QC_Rawread/006T_R1_001.fastq
cat QC_Rawread/E06T_R2_001.fastq  >> QC_Rawread/006T_R2_001.fastq
###

python ~/pipeline/Exome-seq_Pipline/S04_bwa_aln.py --ref $ref --i_dir QC_Rawread/  --o_dir samfile --t 16
python ~/pipeline/Exome-seq_Pipline/S05_sam_to_bam.py  --i_dir samfile/ --o_dir bamfile --ref $ref
python ~/pipeline/Exome-seq_Pipline/S06_AddReadGroup.py --i_dir bamfile --o_dir AddGroup
python ~/pipeline/Exome-seq_Pipline/S07_MarkDuplicate.py --i_dir AddGroup/ --o_dir MarkDuplicate/

python ~/pipeline/Exome-seq_Pipline/S07.5_GATK_Recalibrator.py --i_dir MarkDuplicate/   --o_dir GATK_recal --ref $ref --t 3 --exon_bed region_bed/S07604514_AllTracks.bed

### variant call
python  ~/pipeline/Exome-seq_Pipline/S08_variant_call.py strelka --i_dir GATK_recal/ --key "*recal.bam" --o_dir strelka_file --ref $ref --t 12
#python  ~/pipeline/Exome-seq_Pipline/S08_variant_call.py varscan --i_dir GATK_recal/ --key "*recal.bam" --o_dir varscan_file --ref genome/ucsc.hg19.fasta --t 6


#python  ~/pipeline/Exome-seq_Pipline/S08_varscan.py   --i_dir co_clean_001-009/ --o_dir varscan_file --ref $ref --t 10

#python ~/pipeline/Exome-seq_Pipline/S09_vcf2maf.py  --i_dir  varscan_file/  --o_dir maf_file --ref $ref --t 10 --key "*Somatic.hc"
