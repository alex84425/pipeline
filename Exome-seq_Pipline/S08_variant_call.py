from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll


def shell_cmd(cmd):
	print(cmd)
	os.system(cmd)

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])


		if self.args.pipeline =="varscan":
			self.varscan_cmd(self.args.i_dir,self.args.o_dir)
		
		elif self.args.pipeline =="strelka":
		
			self.strelka_cmd(self.args.i_dir,self.args.o_dir)
		
#		self.Spilit_Somatic_Gerline()
		pass

	def check_arg(self,args=None):
		"""
		choose varaint calling pipeline first
		1. varscan
		2. strelka 
			(1) Usage and download: "https://zhuanlan.zhihu.com/p/31623352" 
			(2) conda
		"""

		parser = argparse.ArgumentParser(description='python ~/Exome-seq_Pipline/S08_variant_call.py  --i_dir MarkDuplicate/  --key "*T*bam" --o_dir varscan_file/ --ref genome/ucsc.hg19.fasta ')
	
		parser.add_argument(	'--i_dir' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o_dir' ,type=str, 
					help='input directory')
		parser.add_argument(	'--ref' ,type=str, 
					help='input reference.fa')
		parser.add_argument(	'--t' ,type=int, 
					default=20		,
					help='input thread num')
		parser.add_argument(	'--single_normal' ,type=str, 
					default=None		,
					help='multiple tumor sample with one normal sample')

		

		parser.add_argument(	'pipeline' ,type=str, 
					help='input varscan or strelka')
		

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str,
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')

		self.args = parser.parse_args(args)


		print("pipeline: ",self.args.pipeline)

	def file_processing(self):
		file_list = sorted( glob.glob("{}/{}".format( self.args.i_dir, self.args.key )) )
		""" file process"""
		if self.args.include !="":
			file_list =  ll.list_iexclude( file_list, self.args.include , "in" )
		if self.args.exclude !="":
			file_list =  ll.list_iexclude( file_list, self.args.exclude , "ex" )
		
		return file_list



	def strelka_cmd(self, path, output_dir):
		"""
		github source: https://github.com/Illumina/strelka/blob/v2.9.x/docs/userGuide/quickStart.md
		requirement:
			1. python 2.7

		"""
		tmp_output_list = []
#		file_list = glob.glob( "{}/{}".format(self.args.i_dir,  self.args.key )  )
		file_list = self.file_processing()
		file_list = [ file.split("/")[-1] for file in file_list]


#		strelka_path = "/home/alex2/git_file/strelka/strelka-2.8.2.centos5_x86_64/"
#		strelka_path = "/home/shepherd71c/git_file/strelka/strelka-2.9.2.centos6_x86_64"
		strelka_path = "/Local_WORK/alex/git_file/strelka-2.9.2.centos6_x86_64"

		print("file_list",file_list)
		os.system("mkdir -p "+self.args.o_dir)

		# thread method
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 1
		for file in file_list:

			if self.args.single_normal != None:
				normal_bam = self.args.single_normal
			else:
				normal_bam = path+"/"+file.replace("T","N")


			tumor_bam =  path+"/"+file.replace("N","T")
			


			# check bai exsit
			if os.path.isfile(  normal_bam.replace(".bam",".bai") ) == False:
				os.system( "samtools index {}".format(normal_bam)  )
			if os.path.isfile(  tumor_bam.replace(".bam",".bai") ) == False :
				os.system( "samtools index {}".format(tumor_bam)  )


			cmd1 = ("python2.7 {0}/bin/configureStrelkaSomaticWorkflow.py --normalBam {1} --tumorBam {2} --referenceFasta {3} --runDir  {4} --exome  --reportEVSFeatures --config={0}/bin/configureStrelkaSomaticWorkflow.py.ini --snvScoringModelFile={0}/share/config/somaticSNVScoringModels.json --indelScoringModelFile={0}/share/config/somaticIndelScoringModels.json --outputCallableRegions"
				.format(strelka_path,normal_bam, tumor_bam, self.args.ref,file[0:3]+"_output" )			)


			"""
				For somatic calling, it is recommended to provide indel candidates from the Manta SV and indel caller to improve sensitivity to call indels of size 20 or larger:
					--indelCandidates candidateSmallIndels.vcf.gz

				For references with many short contigs, it is strongly recommended to provide callable regions to avoid possible runtime issues:
				--callRegions callable.bed.gz
			"""
			tmp_output = file[0:3]+"_output" 
			cmd2 = "python2.7 {}/runWorkflow.py -m local -j {} ".format( tmp_output, self.args.t)

			tmp_output_list.append(tmp_output)
			os.system( cmd1 )
			os.system( cmd2 )

	
		for file in tmp_output_list:
			cmd = "mv {} {}".format(file, self.args.o_dir)
			cmd2 = "zcat  {}/{}/results/variants/somatic.snvs.vcf.gz > {}/{}.somatic.snvs.vcf".format( self.args.o_dir, file, self.args.o_dir, file[0:3]  )
			print(cmd2)
			cmd3 =  cmd2.replace(".snvs.", ".indels.")
			
			os.system(cmd )
			os.system(cmd2 )
			os.system(cmd3 )


	
		pass



	def varscan_cmd(self, path, output_dir):

		#file_list = os.popen("ls "+path+ " |grep '.bam' |grep -v bai |grep N| grep printread").read().strip().split("\n")
		#file_list = glob.glob("{}/{}".format(self.args.i_dir, self.args.key))
		file_list = self.file_processing()
		file_list =  [ file.split("/")[-1]  for file in sorted(file_list) if "T" in file]

		print("file_list",file_list)
		os.system("mkdir -p "+self.args.o_dir)

		# thread method
		t_list=test_thread.myThread_list(shell_cmd,c= self.args.t)
		print("core_num: ",t_list.core_num)
		t_list.sleep_time = 1
		for file in file_list:



			normal_bam = path+"/"+file.replace("T","N")
			tumor_bam =  path+"/"+file.replace("N","T")
			""" for single normal """
			if self.args.single_normal != None:
				normal_bam = self.args.single_normal
			else:
				normal_bam = path+"/"+file.replace("T","N")


			output_pipeup =self.args.o_dir+"/"+ file[:3]+".pipeup"
			pipeup_cmd = ("samtools mpileup -f {} -q 1 -B {} {} >  {} ;"
						.format(self.args.ref, normal_bam, tumor_bam, output_pipeup ) )

			input_pipeup = output_pipeup
			output_vcf = self.args.o_dir+"/"+file[:3]+".vcf"
#			somatic_cmd = "varscan  somatic {}  {} --mpileup 1 --min-coverage 8 --min-coverage-normal 8 --min-coverage-tumor 6 --min-var-freq 0.10 --min-freq-for-hom 0.75 --normal-purity 1.0 --tumor-purity 1.00 --p-value 0.99 --somatic-p-value 0.05 --strand-filter 0 --output-vcf".format( input_pipeup, output_vcf)
#			print( somatic_cmd)
			cmd_process_snp   = ("varscan  processSomatic {}  --min-tumor-freq 0.10 --max-normal-freq 0.05 --p-value 0.07").format(file[:3]+".vcf.snp" )
			cmd_process_indel = ("varscan processSomatic {}  --min-tumor-freq 0.10 --max-normal-freq 0.05 --p-value 0.07").format(file[:3]+".vcf.indel" )


			pipeup_cmd = ("samtools mpileup -f {} -q 1 -B {} {}  "
						.format(self.args.ref, normal_bam, tumor_bam) )
			varscan_cmd_mp = "varscan  somatic	--mpileup {}  --min-coverage 8 --min-coverage-normal 20 --min-coverage-tumor 10  --min-var-freq 0.10 --min-freq-for-hom 0.75 --normal-purity 1.0 --tumor-purity 1.00 --p-value 0.99 --somatic-p-value 0.05 --strand-filter 0 --output-vcf".format( output_vcf )

#			print( pipeup_cmd + " | " + varscan_cmd_mp )

			print( "{}\n{}\n{}\n{}\n".format( (pipeup_cmd + " | " + varscan_cmd_mp) ,"cd "+self.args.o_dir ,cmd_process_snp, cmd_process_indel )  )
			t_list.add_thread( "{}\n{}\n{}\n{}\n".format( (pipeup_cmd + " | " + varscan_cmd_mp) ,"cd "+self.args.o_dir ,cmd_process_snp, cmd_process_indel )  )



			
		t_list.start_thread()

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


