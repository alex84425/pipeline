from __future__ import print_function
import argparse
import sys
import os
import re
import datetime
import operator
# Fixing random state for reproducibility
import argparse
import sys
import test_thread
import gzip
import glob
import random
import pandas as pd
import numpy as np
#from Bio import SeqIO
sys.path.append("~/pipeline/src_common/")
import  list_lib as ll
#def list_iexclude( l, key,  option = "ex" )


def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


class ob_name:

	def __init__(self):
		self.check_arg(sys.argv[1:])
		self.main()
		
		pass

	def check_arg(self,args=None):
		parser = argparse.ArgumentParser(description='python ~/pipeline/Exome-seq_Pipline/maf_paser.py --i_dir ./ --key "*filter*" --o merge.maf')
	
		parser.add_argument(	'--i' ,type=str, 
					help='input directory')

		parser.add_argument(	'--o' ,type=str, 
					default="",
					help='input directory')

		parser.add_argument(	'--sub_t' ,type=int, 
					default=4,
					help='input directory')

		parser.add_argument(	'--rename_barcode' ,type=str, 
					default="{}",
					help="example: CRC{}_PDX")

		parser.add_argument(	'--exon_bed' ,type=str, 
					default="",
					help='input directory')

		""" filter seletor """
		parser.add_argument(	'--key' ,type=str, 
					default = "*",
					help='input directory')
		parser.add_argument(	'--include' ,type=str,
					default = "",
					help='key include')
		parser.add_argument(	'--exclude' ,type=str,
					default = "",
					help='key exlude')

		
		parser.add_argument(	'--f_f' ,type=int, 
					default = 0,
					help="ex: first 30 file")

		self.args = parser.parse_args(args)

	def main(self):
		meta_df = pd.read_csv("clinical.tsv",sep="\t")
		'''
		print( meta_df["ethnicity"].unique() )
		print( meta_df["race"].unique() )
		print(	meta_df["ajcc_pathologic_stage"].unique() )
		print(  meta_df["primary_diagnosis"].unique() ) 
		input()
		'''
		print( len(meta_df) )
#		meta_df = meta_df[meta_df["race"] == "asian"] 
		print( meta_df["race"].value_counts(normalize=False) )
#		print( len(meta_df) )
#		input()

#		meta_meta_df.drop_duplicates(subset ="case_id", keep = False , inplace = True)
		print( meta_df.columns)

		df = self.read_maf( self.args.i )
#		print( meta_df.describe() ) 
		df = pd.merge( df, meta_df, left_on = "case_id", right_on = "case_id", how = "inner")
		df = self.filter_bed(df)

#		input()

		print( len(df [df["race"] == "asian"] ) )
		print( len(df))
#		input()

#		print(df["Tumor_Sample_Barcode"].unique()	)
		TB_list = list(df["Tumor_Sample_Barcode"].unique()	)
		for i in range(3):
			s1 = random.sample(TB_list, 56) 
			s_df = df[ df["Tumor_Sample_Barcode"].isin(s1) ]
#			print(len(s_df))
			self.count_non_sy(s_df)
#			print()
			output = self.args.i.replace(".maf","_" + str(i+1) + ".maf")
#			s_df.to_csv( output, index =False, sep="\t")

	"""
	ethnicity
	race
	ajcc_pathologic_stage
	primary_diagnosis

	"""

	def count_non_sy(self,df):
		TB_list = list(df["Tumor_Sample_Barcode"].unique()	)
		nonsy_snv_df = self.nonsy_snv(df)
		nonsy_indel_df = self.nonsy_indel(df)
#	   print( nonsy_snv_df )
#	   nonsy_df[""]
		print("total non-syn snv:", len(nonsy_snv_df))
		print("total non-syn indel:", len(nonsy_indel_df))
		print("avg non-syn snv:", round(len(nonsy_snv_df)/ len(TB_list),2 ))
		print("avg non-syn indel:", round(len(nonsy_indel_df)/ len(TB_list), 2 ))

		nonsy_snv_n_df = self.nonsy_snv(df[df['dbSNP_RS'] =="novel"])
		nonsy_indel_n_df = self.nonsy_indel(df[df['dbSNP_RS'] =="novel"])
		print("filter mutation in dbSNP:")
		print("total novel non-syn snv:", len(nonsy_snv_n_df))
		print("total novel non-syn indel:", len(nonsy_indel_n_df))
		print("avg novel non-syn snv:", round(len(nonsy_snv_n_df)/ len(TB_list),2 ))
		print("avg novel non-syn indel:", round(len(nonsy_indel_n_df)/ len(TB_list), 2 ))


	"""
['hispanic or latino' 'not hispanic or latino' 'not reported' 'Dead'  'Alive']
['white' 'asian' 'black or african american' 'not reported'  'american indian or alaska native']
['Stage IIIA' 'Stage I' 'Stage IIIB' 'Stage IIIC' 'Stage IVB' 'Stage II'  'Stage IV' 'Stage III' 'No' 'Not Reported' 'Liver' 'T3a' 'T2' '747']
['Hepatocellular carcinoma, NOS'  'Hepatocellular carcinoma, spindle cell variant' 'stage ii' 'stage i'  'stage iiia' 'stage iiib' 'stage iii' 'stage iv' 'stage iiic'  'Hepatocellular carcinoma, clear cell type' 'stage iva' 'not reported'  '--' 'Combined hepatocellular carcinoma and cholangiocarcinoma']


asian

	"""

	def filter_bed(self, df):
		bed_df = pd.read_csv(self.args.exon_bed, sep = "\t")

		"chr1    30317   30527"

		bed_df = bed_df[ ["chr1", "30317", "30527"] ]
		rename_d = {"chr1":"chr", "30317":"start", "30527":"end"}
		bed_df = bed_df.rename( columns=rename_d )

#		bed: chr11   822360  822639
#		bed_df = bed_df[ bed_df["start"] == 822360] 


		itv = {}

		from interval import interval
		
		i1 = interval[1,2]
		i2 = interval[2,3]
#		print(i2 | i1)
#		input()
		for Chr in list(bed_df["chr"].unique()):
			tmp_df = bed_df[  bed_df["chr"] == Chr ]
			tmp_i = interval[0,0]
			#for row in tmp_df:
			for index, row in tmp_df.iterrows():
#				i2 = interval[row[],3]
#				print( row["start"])
				cur_i =interval[ int(row["start"]), int(row["end"]) ]
#				print( cur_i)
				tmp_i = tmp_i | cur_i
			itv[Chr ] = tmp_i
	
				
		
				
				
#		print(itv)

		def in_itv(x):
#			print( itv[ x["Chromosome"]] )
#			print(  x["Chromosome"] )
#			input()
			if  int(x["Start_Position"]) in  itv[ x["Chromosome"]]:
				return True
			return False

		"""
		bed: chr11   822360  822639
		maf: chr11   822492  822492
		"""		
#		df = df.apply( lambda x:in_itv(x), axis = 1 )
#		df = df[ df["Start_Position"] == 822492 ] 
#		bed_df = bed_df[ bed_df["start"] == 822360] 
		print( "maf_df:", len(df))
		print( "bed_df:", len(bed_df))
		print( len(df))
		df = df[df.apply( lambda x : in_itv(x ), axis = 1 )]
		df.to_csv( self.args.i.replace(".maf","_inbed.maf"), sep="\t", index = False)
		
		print( len(df))
		print("after filter bed")	
		print( len(bed_df))

#		print(bed_df.columns)

		input()
		return df

	def nonsy_snv(self, df):
		v_col =[
			  'Missense_Mutation',
			  'Nonsense_Mutation'
		]
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df

	def nonsy_indel(self, df):
		v_col =[
			  'Frame_Shift_Del',
			  'Frame_Shift_Ins',
			  'In_Frame_Ins',
			  'Splice_Site',
			  'In_Frame_Del']
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df


		
	def group_by_gene(self, merge_df, output, g_col):
		TB_list = list(set(list(merge_df["Tumor_Sample_Barcode"])))
		for i,TB in enumerate( TB_list ) :

			df = merge_df[merge_df[ "Tumor_Sample_Barcode"] ==TB]

			g_df = df.groupby( by  = g_col, as_index=True)["Variant_Classification"].count().to_frame()

#			g_df.sort_values(by=[ "Variant_Classification"  ], inplace = True, ascending = False)
			df = g_df	
			df = df.reset_index()
			col = TB

			merge_col = ":".join(g_col)
			df[col] = df["Variant_Classification"]
			if len(g_col)>=2:
				df[ merge_col ] = df[g_col[0]]  + df[g_col[1]].astype( dtype="str" )
			df = df [ [merge_col,col]] 


			if i == 0:		
				gene_df = df		
			else: 
				gene_df = pd.merge(gene_df, df, left_on = merge_col, right_on = merge_col, how= "outer" )

		""" count to 1 and sum and average """
		print( gene_df.columns)
		gene_df["Sum"] = gene_df.iloc[:,1:].sum(axis = 1)
		gene_df["Avg"] = gene_df["Sum"] / len( TB_list)
		gene_df.sort_values(by=[ "Sum"  ], inplace = True, ascending = False)

		""" output groupby gene """

		if len(g_col)>=2:	
			g_output = output.replace(".maf","_GroupByPos.maf")
		else:
			g_output = output.replace(".maf","_GroupByGene.maf")
		gene_df.to_csv(g_output, sep="\t", index=False)

	

	def split_list(self, l):
		ID_list = [ ele.split("/")[-1][:3] for ele in l]
		ID_list = list(set(ID_list))
		td_l = []
		for ID in ID_list:
			g = [ ele  for ele in l if ID in ele ]

			td_l.append( g )

		return td_l



	def filter_maf(self, df):
		v_col =[ 'Frame_Shift_Del',
			  'Missense_Mutation',
			  'Nonsense_Mutation',
			  'Multi_Hit',
			  'Frame_Shift_Ins',
			  'In_Frame_Ins',
			  'Splice_Site',
			  'In_Frame_Del']
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df
	def padding_barcode(self, df):
		df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].astype(dtype = "str").apply( lambda x: x.zfill(3 )  )
		return df



	def rename_barcode(self, df):

		#df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].apply( lambda x: "CRC{}_PDX".format(x) )
		df["Tumor_Sample_Barcode"] = df["Tumor_Sample_Barcode"].apply( lambda x: self.args.rename_barcode.format(x) )
		print(df["Tumor_Sample_Barcode"] )

		return df


	def read_maf(self, maf):
		maf_df = pd.read_csv(maf, sep = "\t", comment="#")
#		print( maf_df )
		return maf_df
#		input()

		pass

	def nonsy_snv(self, df):
		v_col =[
			  'Missense_Mutation',
			  'Nonsense_Mutation'
		]
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df

	def nonsy_indel(self, df):
		v_col =[
			  'Frame_Shift_Del',
			  'Frame_Shift_Ins',
			  'In_Frame_Ins',
			  'Splice_Site',
			  'In_Frame_Del']
		df =  df[ df["Variant_Classification"].isin(v_col) ]
		return df


""" 
Hugo_Symbol Entrez_Gene_Id  Center  NCBI_Build  Chromosome  Start_Position  End_Position	Strand  Variant_Classification  Variant_Type	Reference_Allele	Tumor_Seq_Allele1   Tumor_Seq_Allele2   dbSNP_RS	dbSNP_Val_Status	Tumor_Sample_Barcode	Matched_Norm_Sample_Barcode Match_Norm_Seq_Allele1  Match_Norm_Seq_Allele2  Tumor_Validation_Allele1	Tumor_Validation_Allele2	Match_Norm_Validation_Allele1   Match_Norm_Validation_Allele2   Verification_Status Validation_Status   Mutation_Status Sequencing_Phase	Sequence_Source Validation_Method   Score   BAM_File	Sequencer   Tumor_Sample_UUID   Matched_Norm_Sample_UUID	HGVSc   HGVSp   HGVSp_Short Transcript_ID   Exon_Number t_depth t_ref_count t_alt_count n_depth n_ref_count n_alt_count all_effects Allele  Gene	Feature Feature_type	Consequence cDNA_position   CDS_position	Protein_position	Amino_acids Codons  Existing_variation  ALLELE_NUM  DISTANCE	STRAND_VEP  SYMBOL  SYMBOL_SOURCE   HGNC_ID BIOTYPE CANONICAL   CCDS	ENSP	SWISSPROT   TREMBL  UNIPARC RefSeq  SIFT	PolyPhen	EXON	INTRON  DOMAINS AF  AFR_AF  AMR_AF  ASN_AF  EAS_AF  EUR_AF  SAS_AF  AA_AF   EA_AF   CLIN_SIG	SOMATIC PUBMED  MOTIF_NAME  MOTIF_POS   HIGH_INF_POS	MOTIF_SCORE_CHANGE  IMPACT  PICK	VARIANT_CLASS   TSL HGVS_OFFSET PHENO   MINIMISED   ExAC_AF ExAC_AF_AFR ExAC_AF_AMR ExAC_AF_EAS ExAC_AF_FIN ExAC_AF_NFE ExAC_AF_OTH ExAC_AF_SAS GENE_PHENO  FILTER  flanking_bps	vcf_id  vcf_qual	ExAC_AF_Adj ExAC_AC_AN_Adj  ExAC_AC_AN  ExAC_AC_AN_AFR  ExAC_AC_AN_AMR  ExAC_AC_AN_EAS  ExAC_AC_AN_FIN  ExAC_AC_AN_NFE  ExAC_AC_AN_OTH  ExAC_AC_AN_SAS  ExAC_FILTER gnomAD_AF   gnomAD_AFR_AF   gnomAD_AMR_AF   gnomAD_ASJ_AF   gnomAD_EAS_AF   gnomAD_FIN_AF   gnomAD_NFE_AF   gnomAD_OTH_AF   gnomAD_SAS_AF   vcf_pos

"""

if __name__ == '__main__':

	ob=ob_name()
	#assign variable


